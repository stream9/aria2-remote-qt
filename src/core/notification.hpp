#ifndef ARIA2_REMOTE_QT5_CORE_NOTIFICATION_HPP
#define ARIA2_REMOTE_QT5_CORE_NOTIFICATION_HPP

#include <QString>
#include <QObject>

namespace aria2_remote {

class Application;
class Aria2Remote;

class Notification : public QObject
{
public:
    Notification(Application&);

private:
    Q_SLOT void onAria2Changed(Aria2Remote*);
    Q_SLOT void onDownloadStart(QString const& gid);
    Q_SLOT void onDownloadPause(QString const& gid);
    Q_SLOT void onDownloadStop(QString const& gid);
    Q_SLOT void onDownloadComplete(QString const& gid);
    Q_SLOT void onDownloadError(QString const& gid);
    Q_SLOT void onBtDownloadComplete(QString const& gid);

    void launchCommand(QString const& method, QString const& gid);

private:
    Application& m_application;
    QString m_command;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_QT5_CORE_NOTIFICATION_HPP
