#ifndef ARIA2_REMOTE_DOWNLOAD_PEERS_HPP
#define ARIA2_REMOTE_DOWNLOAD_PEERS_HPP

#include "bit_field.hpp"

#include <cstdint>
#include <memory>
#include <vector>

#include <QObject>
#include <QString>
#include <QTimer>

class QJsonArray;
class QJsonObject;

namespace aria2_remote {

class DownloadHandle;

class Peer
{
public:
    Peer(QJsonObject const&, DownloadHandle const&);

    QString const& id() const { return m_id; }
    QString const& address() const { return m_address; }
    uint16_t port() const { return m_port; }
    BitField const& bitField() const { return m_bitField; }
    int32_t downloadSpeed() const { return m_downloadSpeed; }
    int32_t uploadSpeed() const { return m_uploadSpeed; }
    bool isChoking() const { return m_choking; }
    bool isChoked() const { return m_choked; }
    bool isSeeder() const { return m_seeder; }

    bool operator==(Peer const& other) const;
    bool operator!=(Peer const& other) const { return !(*this == other); }

private:
    QString m_id;
    QString m_address;
    uint16_t m_port;
    BitField m_bitField;
    int32_t m_downloadSpeed;
    int32_t m_uploadSpeed;
    bool m_choking;
    bool m_choked;
    bool m_seeder;
};

class DownloadPeers : public QObject
                    , public std::enable_shared_from_this<DownloadPeers>
{
    Q_OBJECT
    using Peers = std::vector<Peer>;

public:
    DownloadPeers(DownloadHandle&);

    auto operator[](size_t const index) { return m_peers.at(index); }

    auto size() const { return m_peers.size(); }

    auto begin() const { return m_peers.begin(); }
    auto end() const { return m_peers.end(); }

    Q_SIGNAL void peerChanged(size_t, Peer const&) const;
    Q_SIGNAL void peerCreated(size_t, Peer const&) const;
    Q_SIGNAL void peerDeleted(size_t, Peer const&) const;

    Q_SIGNAL void cleared() const;

private:
    Q_SLOT void update();
    Q_SLOT void onHandleDestroyed();

    Peers createPeers(QJsonArray const&) const;
    void updatePeers(Peers&);
    void clear();

private:
    DownloadHandle* m_handle; // nullable
    QTimer m_timer;

    Peers m_peers;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_DOWNLOAD_PEERS_HPP
