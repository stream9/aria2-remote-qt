#ifndef ARIA2_REMOTE_OFFSET_MODE_HPP
#define ARIA2_REMOTE_OFFSET_MODE_HPP

class QString;

namespace aria2_remote {

class OffsetMode
{
public:
    enum Value { Set = 0, Current, End, Count };

public:
    OffsetMode(Value const);

    QString const& rpcString() const;

private:
    Value m_value;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_OFFSET_MODE_HPP
