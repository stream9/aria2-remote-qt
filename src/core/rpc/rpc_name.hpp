#ifndef ARIA2_REMOTE_RPC_NAME_HPP
#define ARIA2_REMOTE_RPC_NAME_HPP

class QString;

namespace aria2_remote::rpc_name {

QString addUri();
QString addTorrent();
QString addMetalink();
QString remove();
QString forceRemove();
QString pause();
QString pauseAll();
QString forcePause();
QString forcePauseAll();
QString unpause();
QString unpauseAll();
QString tellActive();
QString tellWaiting();
QString tellStopped();
QString getUris();
QString getPeers();
QString getServers();
QString changePosition();
QString getOption();
QString changeOption();
QString getGlobalOption();
QString changeGlobalOption();
QString getGlobalStat();
QString removeDownloadResult();
QString multiCall();

} // namespace aria2_remote::rpc_name

#endif // ARIA2_REMOTE_RPC_NAME_HPP
