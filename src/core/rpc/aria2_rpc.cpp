#include "aria2_rpc.hpp"

#include "multi_call.hpp"
#include "rpc_name.hpp"

#include "json_rpc/web_socket.hpp"
#include "utility/json.hpp"
#include "make_parameters.hpp"

#include <cassert>

#include <QJsonArray>
#include <QJsonObject>
#include <QString>
#include <QUrl>

#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonObject>
#include <QJsonArray>
#include <QDebug>

namespace aria2_remote {

using util::toObject;

Aria2Error::
Aria2Error(QJsonValue const& err)
{
    assert(err.isObject());
    auto const& obj = err.toObject();

    assert(obj.contains("code"));
    auto const& code = obj.value("code");
    assert(code.isDouble());
    this->code = code.toInt();

    assert(obj.contains("message"));
    auto const& message = obj.value("message");
    assert(message.isString());
    this->message = message.toString();
}


Aria2Rpc::
Aria2Rpc(QUrl const& url)
    : m_jsonRpc { std::make_unique<json_rpc::WebSocket>(url) }
{
    this->connect(m_jsonRpc.get(), &json_rpc::WebSocket::connected,
                  this,            &Aria2Rpc::connected);
    this->connect(m_jsonRpc.get(), &json_rpc::WebSocket::disconnected,
                  this,            &Aria2Rpc::disconnected);
    this->connect(m_jsonRpc.get(), &json_rpc::WebSocket::error,
                  this,            &Aria2Rpc::error);
    this->connect(m_jsonRpc.get(), &json_rpc::WebSocket::notification,
                  this,            &Aria2Rpc::notification);
    this->connect(m_jsonRpc.get(), &json_rpc::WebSocket::notification,
                  this,            &Aria2Rpc::onNotification);
}

Aria2Rpc::~Aria2Rpc() = default;

bool Aria2Rpc::
isValid() const
{
    return m_jsonRpc->isValid();
}

void Aria2Rpc::
addUri(QJsonArray const& uris, Callback const& callback)
{
    auto const& params = makeParameters(uris);

    callAria2(rpc_name::addUri(), params, callback);
}

void Aria2Rpc::
addUri(QJsonArray const& uris, QJsonObject const& options,
       Callback const& callback)
{
    auto const& params = makeParameters(uris, options);

    callAria2(rpc_name::addUri(), params, callback);
}

void Aria2Rpc::
addUri(QJsonArray const& uris, QJsonObject const& options,
       int32_t const position, Callback const& callback)
{
    assert(position >= 0);

    auto const& params = makeParameters(uris, options, position);

    callAria2(rpc_name::addUri(), params, callback);
}

void Aria2Rpc::
addTorrent(QString const& torrent, Callback const& callback)
{
    auto const& params = makeParameters(torrent);

    callAria2(rpc_name::addTorrent(), params, callback);
}

void Aria2Rpc::
addTorrent(QString const& torrent,
           QJsonArray const& uris, Callback const& callback)
{
    auto const& params = makeParameters(torrent, uris);

    callAria2(rpc_name::addTorrent(), params, callback);
}

void Aria2Rpc::
addTorrent(QString const& torrent,
           QJsonArray const& uris,
           QJsonObject const& options, Callback const& callback)
{
    auto const& params = makeParameters(torrent, uris, options);

    callAria2(rpc_name::addTorrent(), params, callback);
}

void Aria2Rpc::
addTorrent(QString const& torrent,
           QJsonArray const& uris,
           QJsonObject const& options,
           int32_t position, Callback const& callback)
{
    auto const& params = makeParameters(torrent, uris, options, position);

    callAria2(rpc_name::addTorrent(), params, callback);
}

void Aria2Rpc::
addMetalink(QString const& metalink, Callback const& callback)
{
    auto const& params = makeParameters(metalink);

    callAria2(rpc_name::addMetalink(), params, callback);
}

void Aria2Rpc::
addMetalink(QString const& metalink,
            QJsonObject const& options, Callback const& callback)
{
    auto const& params = makeParameters(metalink, options);

    callAria2(rpc_name::addMetalink(), params, callback);
}

void Aria2Rpc::
addMetalink(QString const& metalink, QJsonObject const& options,
            int32_t position, Callback const& callback)
{
    auto const& params = makeParameters(metalink, options, position);

    callAria2(rpc_name::addMetalink(), params, callback);
}

void Aria2Rpc::
remove(QString const& gid, Callback const& callback)
{
    auto const& params = makeParameters(gid);

    callAria2(rpc_name::remove(), params, callback);
}

void Aria2Rpc::
forceRemove(QString const& gid, Callback const& callback)
{
    auto const& params = makeParameters(gid);

    callAria2(rpc_name::forceRemove(), params, callback);
}

void Aria2Rpc::
pause(QString const& gid, Callback const& callback)
{
    auto const& params = makeParameters(gid);

    callAria2(rpc_name::pause(), params, callback);
}

void Aria2Rpc::
pauseAll(Callback const& callback)
{
    QJsonArray params;

    callAria2(rpc_name::pauseAll(), params, callback);
}

void Aria2Rpc::
forcePause(QString const& gid, Callback const& callback)
{
    auto const& params = makeParameters(gid);

    callAria2(rpc_name::forcePause(), params, callback);
}

void Aria2Rpc::
forcePauseAll(Callback const& callback)
{
    QJsonArray params;

    callAria2(rpc_name::forcePauseAll(), params, callback);
}

void Aria2Rpc::
unpause(QString const& gid, Callback const& callback)
{
    auto const& params = makeParameters(gid);

    callAria2(rpc_name::unpause(), params, callback);
}

void Aria2Rpc::
unpauseAll(Callback const& callback)
{
    QJsonArray params;

    callAria2(rpc_name::unpauseAll(), params, callback);
}

void Aria2Rpc::
tellActive(Callback const& callback)
{
    QJsonArray params;

    callAria2(rpc_name::tellActive(), params, callback);
}

void Aria2Rpc::
tellWaiting(int32_t const offset, int32_t const num, Callback const& callback)
{
    auto const& params = makeParameters(offset, num);

    callAria2(rpc_name::tellWaiting(), params, callback);
}

void Aria2Rpc::
tellStopped(int32_t const offset, int32_t const num, Callback const& callback)
{
    auto const& params = makeParameters(offset, num);

    callAria2(rpc_name::tellStopped(), params, callback);
}

void Aria2Rpc::
getUris(QString const& gid, Callback const& callback)
{
    auto const& params = makeParameters(gid);

    callAria2(rpc_name::getUris(), params, callback);
}

void Aria2Rpc::
getPeers(QString const& gid, Callback const& callback)
{
    auto const& params = makeParameters(gid);

    callAria2(rpc_name::getPeers(), params, callback);
}

void Aria2Rpc::
getServers(QString const& gid, Callback const& callback)
{
    auto const& params = makeParameters(gid);

    callAria2(rpc_name::getServers(), params, callback);
}

void Aria2Rpc::
changePosition(QString const& gid, int32_t const pos,
                        OffsetMode const offset, Callback const& callback)
{
    auto const& params = makeParameters(gid, pos, offset.rpcString());

    callAria2(rpc_name::changePosition(), params, callback);
}

void Aria2Rpc::
getOption(QString const& gid, Callback const& callback)
{
    auto const& params = makeParameters(gid);

    callAria2(rpc_name::getOption(), params, callback);
}

void Aria2Rpc::
changeOption(QString const& gid,
             QJsonObject const& options, Callback const& callback)
{
    auto const& params = makeParameters(gid, options);

    callAria2(rpc_name::changeOption(), params, callback);
}

void Aria2Rpc::
getGlobalOption(Callback const& callback)
{
    QJsonArray params;

    callAria2(rpc_name::getGlobalOption(), params, callback);
}

void Aria2Rpc::
changeGlobalOption(QJsonObject const& options, Callback const& callback)
{
    auto const& params = makeParameters(options);

    callAria2(rpc_name::changeGlobalOption(), params, callback);
}

void Aria2Rpc::
getGlobalStat(Callback const& callback)
{
    QJsonArray params;

    callAria2(rpc_name::getGlobalStat(), params, callback);
}

void Aria2Rpc::
removeDownloadResult(QString const& gid, Callback const& callback)
{
    auto const& params = makeParameters(gid);

    callAria2(rpc_name::removeDownloadResult(), params, callback);
}

MultiCall Aria2Rpc::
multiCall()
{
    return MultiCall { *this };
}

void Aria2Rpc::
onNotification(QString const& method, QJsonValue const& params)
{
    //qDebug() << "Notification:" << method << util::toJson(params);

    auto const& paramsArr = params.toArray();
    if (paramsArr.size() != 1) return;

    auto const param = paramsArr[0].toObject();
    if (!param.contains("gid")) return;

    auto const& gid = param["gid"].toString();
    if (gid.isEmpty()) return;

    if (method == "aria2.onDownloadStart") {
        Q_EMIT onDownloadStart(gid);
    }
    else if (method == "aria2.onDownloadPause") {
        Q_EMIT onDownloadPause(gid);
    }
    else if (method == "aria2.onDownloadStop") {
        Q_EMIT onDownloadStop(gid);
    }
    else if (method == "aria2.onDownloadComplete") {
        Q_EMIT onDownloadComplete(gid);
    }
    else if (method == "aria2.onDownloadError") {
        Q_EMIT onDownloadError(gid);
    }
    else if (method == "aria2.onBtDownloadComplete") {
        Q_EMIT onBtDownloadComplete(gid);
    }
}

void Aria2Rpc::
callAria2(QString const& method, QJsonArray const& params, Callback const& callback)
{
    m_jsonRpc->call(method, params,
        [callback](auto const& res) {
            callback(res);
        }
    );
}

} // namespace aria2_remote
