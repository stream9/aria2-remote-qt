#ifndef ARIA2_REMOTE_MULTI_CALL_HPP
#define ARIA2_REMOTE_MULTI_CALL_HPP

#include "aria2_rpc.hpp"
#include "make_parameters.hpp"

#include <vector>

#include <QString>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>

namespace aria2_remote {

class MultiCall
{
public:
    using Callback = Aria2Rpc::Callback;

    using Results = std::vector<Aria2Rpc::Response>;

public:
    MultiCall(Aria2Rpc& aria2);

    bool empty() const;
    QString toJson() const;

    void exec(Callback const&);

    MultiCall& tellActive();
    MultiCall& tellWaiting(int offset, int num);
    MultiCall& tellStopped(int offset, int num);
    MultiCall& getGlobalOption();
    MultiCall& getGlobalStat();

    //MultiCall& remove(QString const& gid);  // It doesn't seem to work, aria2 just return empty response.

    static Results unpackResult(QJsonValue const&);

private:
    template<typename... Args>
    void pushMethod(QString const& name, Args&&... args)
    {
        auto const& params = makeParameters(std::forward<Args>(args)...);

        m_methods.push_back(makeMethod(name, params));
    }

    QJsonObject makeMethod(QString const& name, QJsonArray const& params);

private:
    Aria2Rpc& m_aria2;
    QJsonArray m_methods;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_MULTI_CALL_HPP
