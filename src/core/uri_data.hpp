#ifndef ARIA2_REMOTE_URI_DATA_HPP
#define ARIA2_REMOTE_URI_DATA_HPP

#include <QString>

class QJsonObject;
class QJsonValue;

namespace aria2_remote {

class UriData
{
public:
    enum UriStatus { URI_USED = 0, URI_WAITING };

public:
    UriData(QJsonObject const&);

    QString uri() const { return m_uri; }
    UriStatus status() const { return m_status; }
    QString const& statusText() const;

    bool update(QJsonObject const&);

private:
    bool setUri(QJsonValue const&);
    bool setStatus(QJsonValue const&);

    friend QDebug operator<<(QDebug, UriData const&);

private:
    QString m_uri;
    UriStatus m_status;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_URI_DATA_HPP
