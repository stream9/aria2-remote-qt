#include "file_data.hpp"

#include "utility/json.hpp"

#include <cassert>

#include <boost/numeric/conversion/cast.hpp>

#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>

#include <QDebug>

namespace aria2_remote {

using util::toInt32;
using util::toInt64;
using util::toBool;
using util::toString;
using util::toArray;

template<typename T>
static bool
updateValue(T const& src, T& dest)
{
    if (src != dest) {
        dest = src;
        return true;
    }
    return false;
}

// FileData

FileData::
FileData(QJsonObject const& obj)
{
    update(obj);
}

bool FileData::
update(QJsonObject const& obj)
{
    bool changed = false;

    changed |= setIndex(obj["index"]);
    changed |= setPath(obj["path"]);
    changed |= setLength(obj["length"]);
    changed |= setCompletedLength(obj["completedLength"]);
    changed |= setSelected(obj["selected"]);
    changed |= setUris(obj["uris"]);

    return changed;
}

bool FileData::
setIndex(QJsonValue const& value)
{
    return updateValue(toInt32(value), m_index);
}

bool FileData::
setPath(QJsonValue const& value)
{
    return updateValue(toString(value), m_path);
}

bool FileData::
setLength(QJsonValue const& value)
{
    return updateValue(toInt64(value), m_length);
}

bool FileData::
setCompletedLength(QJsonValue const& value)
{
    return updateValue(toInt64(value), m_completedLength);
}

bool FileData::
setSelected(QJsonValue const& value)
{
    return updateValue(toBool(value), m_selected);
}

bool FileData::
setUris(QJsonValue const& value)
{
    auto const& uris = toArray(value);

    bool changed = false;

    if (boost::numeric_cast<size_t>(uris.size()) == m_uris.size()) {
        for (qsizetype idx = 0, len = uris.size(); idx < len; ++idx) {
            auto const& uri = uris[idx];
            assert(uri.isObject());

            changed |= m_uris[static_cast<size_t>(idx)].update(uri.toObject());
        }
    }
    else {
        m_uris.clear();
        for (auto const& uri: uris) {
            assert(uri.isObject());

            m_uris.emplace_back(uri.toObject());
        }
        changed = true;
    }

    return changed;
}

QDebug
operator<<(QDebug dbg, FileData const& data)
{
    static QString fmt { "%1%2: %3 %4/%5\n" };

    dbg.noquote()
        << fmt.arg(data.m_selected ? "*" : " ")
              .arg(data.m_index)
              .arg(data.m_path)
              .arg(data.m_completedLength)
              .arg(data.m_length)
        ;

    for (auto const& uri: data.m_uris) {
        dbg << uri << "\n";
    }

    return dbg;
}

} // namespace aria2_remote
