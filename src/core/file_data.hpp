#ifndef ARIA2_REMOTE_FILE_DATA_HPP
#define ARIA2_REMOTE_FILE_DATA_HPP

#include "uri_data.hpp"

#include <vector>

#include <QString>

class QJsonObject;
class QJsonValue;

namespace aria2_remote {

class FileData
{
public:
    using UriDatas = std::vector<UriData>;

public:
    FileData(QJsonObject const&);

    int32_t index() const { return m_index; }
    QString path() const { return m_path; }
    int64_t length() const { return m_length; }
    int64_t completedLength() const { return m_completedLength; }
    bool selected() const { return m_selected; }
    UriDatas const& uris() const { return m_uris; }

    bool update(QJsonObject const&);

private:
    bool setIndex(QJsonValue const&);
    bool setPath(QJsonValue const&);
    bool setLength(QJsonValue const&);
    bool setCompletedLength(QJsonValue const&);
    bool setSelected(QJsonValue const&);
    bool setUris(QJsonValue const&);

    friend QDebug operator<<(QDebug, FileData const&);

private:
    int32_t m_index;
    QString m_path;
    int64_t m_length;
    int64_t m_completedLength;
    bool m_selected;
    UriDatas m_uris;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_FILE_DATA_HPP
