#include "global_options.hpp"

#include "aria2_remote.hpp"
#include "rpc/aria2_rpc.hpp"
#include "json_rpc/response.hpp"
#include "utility/json.hpp"

#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>

namespace aria2_remote {

using util::toString;

GlobalOptions::
GlobalOptions(Aria2Remote& aria2)
    : m_aria2 { aria2 }
{}

void GlobalOptions::
changeOption(QJsonObject const& options)
{
    m_aria2.rpc().changeGlobalOption(options,
        [this, options](auto const& res) {
            if (res.isError()) {
                qWarning() << "fail to change global option"
                           << QJsonDocument(options).toJson();
            }
            else {
                refresh();
            }
        }
    );
}

void GlobalOptions::
getOption(Callback const& callback)
{
    m_aria2.rpc().getGlobalOption(callback);
}

} // namespace aria2_remote
