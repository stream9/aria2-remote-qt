#include "bit_field.hpp"

#include <QDebug>
#include <QString>

namespace aria2_remote {

static uint8_t
ctoi(char const ch)
{
    uint8_t result = 0;

    if ('0' <= ch && ch <= '9') {
        result = static_cast<uint8_t>(ch - '0');
    }
    else if ('a' <= ch && ch <= 'f') {
        result = static_cast<uint8_t>(10 + ch - 'a');
    }
    else if ('A' <= ch && ch <= 'F') {
        result = static_cast<uint8_t>(10 + ch - 'A');
    }
    else {
        assert(false);
    }

    assert(result < 16);
    return result;
}

BitField::
BitField(QString const& chars, int32_t const size)
{
    assert(size >= 0);
    this->reserve(static_cast<size_t>(chars.size() * 4));

    for (auto const c: chars) {
        auto const ch = ctoi(c.toLatin1());

        for (auto i = 3; i >= 0; --i) {
            this->push_back(ch & (1 << i));
        }
    }

    this->resize(static_cast<size_t>(size));
}

QDebug
operator<<(QDebug dbg, BitField const& bitField)
{
    QDebugStateSaver state { dbg };

    dbg.nospace();
    for (auto i = 0u; i < bitField.size(); ++i) {
        dbg << (bitField[i] ? "1" : "0");
    }

    return dbg;
}

} // namespace aria2_remote
