#ifndef ARIA2_REMOTE_BIT_FIELD_HPP
#define ARIA2_REMOTE_BIT_FIELD_HPP

#include <cstdint>

#include <boost/dynamic_bitset.hpp>

class QString;
class QDebug;

namespace aria2_remote {

class BitField : public boost::dynamic_bitset<>
{
public:
    BitField() = default;
    BitField(QString const& chars, int32_t const size);
};

QDebug operator<<(QDebug, BitField const&);

} // namespace aria2_remote

#endif // ARIA2_REMOTE_BIT_FIELD_HPP
