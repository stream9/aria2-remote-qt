#include "command_line.hpp"

#include <cassert>
#include <memory>
#include <iostream>

#include <unistd.h>

#include <QApplication>

namespace aria2_remote {

static std::string
toString(QString const& s)
{
    auto const& buf = s.toUtf8();
    return { buf.constData(), static_cast<size_t>(buf.size()) };
}

static void
printVersion()
{
    std::cout << toString(qApp->applicationName())
              << " version "
              << toString(qApp->applicationVersion())
              << "\n";
}

std::unique_ptr<CommandLine> s_commandLine;

CommandLine::
CommandLine(int argc, char* argv[])
    : m_argc { argc }
    , m_argv { argv }
{
    int c;

    while ((c = getopt(argc, argv, "hv")) != -1) {
        switch (c) {
            case 'h':
                printUsage();
                exit(0);
                break;
            case 'v':
                printVersion();
                exit(0);
                break;
            case '?': {
                std::string msg { "unknown option: -" };
                msg.push_back(static_cast<char>(optopt));
                std::cerr << msg;
                exit(1);
            }
            default:
                abort();
        }
    }

    if (optind != argc) {
        if (argc - optind > 1) {
            printUsage();
            exit(0);
        }
        m_server = m_argv[optind];
    }
}

void CommandLine::
printUsage() const
{
    std::cout << "Usage: " << m_argv[0] << " [OPTIONS] [server]\n"
              << "Options\n"
              << "    -h print this message\n"
              ;
}

void
parseCommandLine(int argc, char* argv[])
{
    s_commandLine = std::make_unique<CommandLine>(argc, argv);
}

CommandLine const&
gCommandLine()
{
    assert(s_commandLine);

    return *s_commandLine;
}

} // namespace aria2_remote
