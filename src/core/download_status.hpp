#ifndef ARIA2_REMOTE_DOWNLOAD_STATUS_HPP
#define ARIA2_REMOTE_DOWNLOAD_STATUS_HPP

#include <cstdint>

#include <QString>

class QJsonValue;

namespace aria2_remote {

class DownloadStatus
{
public:
    enum Value : int8_t {
        Active = 0,
        Waiting,
        Paused,
        Complete,
        Error,
        Removed,
        Invalid
    };
public:
    DownloadStatus();

    bool isValid() const;

    QString const& text() const;

    operator Value() const { return m_value; }

    bool update(QJsonValue const&);

private:
    Value m_value;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_DOWNLOAD_STATUS_HPP
