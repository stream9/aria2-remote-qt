#ifndef ARIA2_REMOTE_PROFILE_HPP
#define ARIA2_REMOTE_PROFILE_HPP

#include "options.hpp"

#include <QString>

class QJsonObject;

namespace aria2_remote {

// Profile

class Profile : public Options
{
public:
    Profile(QString const& name);

private:
    void changeOption(QJsonObject const& options) override;
    void getOption(Callback const&) override;

private:
    QString m_name;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_PROFILE_HPP
