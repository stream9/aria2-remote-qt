#include "server.hpp"

#include "application.hpp"
#include "aria2_remote.hpp"
#include "settings.hpp"

#include "utility/pointer.hpp"

#include <algorithm>

#include <QUrl>

namespace aria2_remote {

// Server
Server::
Server(QString const& name,
       QString const& host,
       uint16_t port,
       QString const& secret)
    : m_name { name }
    , m_host { host }
    , m_secret { secret }
    , m_port { port }
{}

Server::~Server() = default;

Aria2Remote* Server::
aria2() const
{
    if (m_aria2 && m_aria2->isActive()) {
        return m_aria2.get();
    }
    else {
        return nullptr;
    }
}

QUrl Server::
url() const
{
    QString const fmt { "ws://%1:%2/jsonrpc" };
    return { fmt.arg(m_host).arg(m_port) };
}

void Server::
connect()
{
    m_aria2 = std::make_shared<Aria2Remote>(*this);

    QObject::connect(m_aria2.get(), &Aria2Remote::initialized,
                     this,          &Server::connected);
    QObject::connect(m_aria2.get(), &Aria2Remote::deactivated,
                     this,          &Server::disconnected);
    QObject::connect(m_aria2.get(), &Aria2Remote::error,
                     this,          &Server::connectionError);
}

void Server::
disconnect()
{
    m_aria2.reset();

    Q_EMIT disconnected();
}

void Server::
assign(QString const& name,
       QString const& host,
       uint16_t const port,
       QString const& secret)
{
    m_name = name;
    m_host = host;
    m_secret = secret;
    m_port = port;

    if (isConnected()) {
        disconnect();
        connect();
    }

    Q_EMIT edited();
}

bool Server::
isConnected() const
{
    return m_aria2 && m_aria2->isActive();
}

// Servers

Servers::
Servers(Application& app)
    : m_application { app }
{
    load();
}

Servers::
~Servers()
{
    save();
}

Server const* Servers::
find(QString const& name) const
{
    auto const it = std::find_if(m_servers.begin(), m_servers.end(),
        [&](auto const& server) {
            return server.name() == name;
        });

    if (it != m_servers.end()) {
        return &*it;
    }
    else {
        return nullptr;
    }
}

Server* Servers::
find(QString const& name)
{
    return const_cast<Server*>(
        const_cast<Servers const*>(this)->find(name)
    );
}

void Servers::
append(QString const& name,
       QString const& host,
       uint16_t port,
       QString const& secret)
{
    m_servers.push_back(new Server { name, host, port, secret });
    auto& server = m_servers.back();

    this->connect(&server, &Server::edited,
                  this,    &Servers::onEdited);
    this->connect(&server, &Server::connectionError,
                  this,    &Servers::onConnectionError);

    Q_EMIT this->appended();
}

void Servers::
remove(Server const& server)
{
    auto const it = std::find_if(m_servers.begin(), m_servers.end(),
        [&](auto const& item) {
            return &item == &server;
        });

    auto const index = std::distance(m_servers.begin(), it);

    m_servers.erase(it);

    Q_EMIT removed(static_cast<size_t>(index));
}

void Servers::
load()
{
    auto& settings = m_application.settings();

    auto const size = settings.beginReadArray("Servers");

    for (auto i = 0; i < size; ++i) {
        settings.setArrayIndex(i);
        append(settings.value("name").toString()
             , settings.value("host").toString()
             , settings.value("port").value<uint16_t>()
             , settings.value("secret").toString()
        );
    }

    settings.endArray();
}

void Servers::
save() const
{
    auto& settings = m_application.settings();

    settings.beginWriteArray("Servers");

    for (size_t i = 0; i < m_servers.size(); ++i) {
        auto& server = m_servers[i];

        settings.setArrayIndex(static_cast<int>(i));

        settings.setValue("name", server.name());
        settings.setValue("host", server.host());
        settings.setValue("port", server.port());
        settings.setValue("secret", server.secret());
    }

    settings.endArray();
}

void Servers::
onEdited() const
{
    auto& server = to_ref(dynamic_cast<Server*>(this->sender()));

    auto const it = std::find_if(m_servers.begin(), m_servers.end(),
        [&](auto const& item) {
            return &item == &server;
        });
    assert(it != m_servers.end());

    auto const index = std::distance(m_servers.begin(), it);

    Q_EMIT edited(static_cast<size_t>(index));
}

void Servers::
onConnectionError(QString const& msg) const
{
    auto& server = to_ref(dynamic_cast<Server*>(this->sender()));

    Q_EMIT connectionError(server, msg);
}

} // namespace aria2_remote
