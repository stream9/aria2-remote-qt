#ifndef ARIA2_REMOTE_DOWNLOAD_URIS_HPP
#define ARIA2_REMOTE_DOWNLOAD_URIS_HPP

#include "uri_data.hpp"

#include <memory>
#include <vector>

#include <QObject>

class QJsonValue;

namespace aria2_remote {

class DownloadHandle;

class DownloadUris : public QObject
{
    Q_OBJECT
public:
    using UriDatas = std::vector<UriData>;

public:
    DownloadUris(DownloadHandle&);

    UriDatas const& uris() const { return m_uris; }

    void update();

    Q_SIGNAL void changed() const;

private:
    void setUris(QJsonValue const&);
    void clear();

private:
    DownloadHandle& m_handle;

    UriDatas m_uris;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_DOWNLOAD_URIS_HPP
