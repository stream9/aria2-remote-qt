#include "download_handles.hpp"

#include "aria2_remote.hpp"
#include "download_handle.hpp"
#include "rpc/aria2_rpc.hpp"

#include "json_rpc/response.hpp"
#include "utility/json.hpp"

#include <algorithm>
#include <cassert>
#include <vector>

#include <boost/numeric/conversion/cast.hpp>

#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>

#include <QDebug>

#include <stream9/myers_diff.hpp>

namespace aria2_remote {

using util::toArray;
using util::toObject;
using util::toString;

namespace myers_diff = stream9::myers_diff;

static QString
getStatusGid(QJsonObject const& status)
{
    return toString(status.value("gid"));
}

DownloadHandles::
DownloadHandles(Aria2Remote& aria2)
    : m_aria2 { aria2 }
{}
#if 0
template<typename H, typename S>
static void
verifyUpdate(H const& before, H const& after, S const& statuses)
{
    (void)before;

    auto const equals = std::equal(
        after.begin(), after.end(),
        statuses.begin(), statuses.end(),
        [](auto const& handle, auto const& status) {
            return handle->gid() == toString(toObject(status).value("gid"));
        }
    );

    auto printHandle = [](auto const& handle) {
        qDebug() << handle->gid();
    };

    auto printStatus = [](auto const& status) {
        qDebug() << toString(toObject(status).value("gid"));
    };

    if (!equals) {
        qDebug() << "discrepancy detected";
        qDebug() << "BEFORE";
        std::for_each(before.begin(), before.end(), printHandle);
        qDebug() << "AFTER";
        std::for_each(after.begin(), after.end(), printHandle);
        qDebug() << "STATUS";
        std::for_each(statuses.begin(), statuses.end(), printStatus);
    }
}
#endif

struct diff {
    enum type_t { same, insert, remove };
    type_t type;
    size_t index;
};

static std::vector<diff>
generateDiff(auto& before, auto& after)
{
    struct Visitor : myers_diff::event_handler
    {
        using index_t = myers_diff::uindex_t;

        Visitor(std::vector<diff>& diffs) noexcept
            : m_diffs { diffs }
        {}

        void unchanged(index_t /*index1*/, index_t index2) noexcept override
        {
            m_diffs.push_back({
                diff::same,
                index2,
            });
        }

        void inserted(index_t index) noexcept override
        {
            m_diffs.push_back({
                diff::insert,
                index,
            });
        }

        void deleted(index_t index) noexcept override
        {
            m_diffs.push_back({
                diff::remove,
                index,
            });
        }

        std::vector<diff>& m_diffs;
    };

    std::vector<diff> diffs;
    Visitor visitor { diffs };

    auto equal = [&](auto index1, auto index2) {
        auto&& st = toObject(after[static_cast<int>(index2)]);
       return before[index1]->gid() == getStatusGid(st);
    };

    myers_diff::diff(
        before.size(),
        static_cast<size_t>(after.size()),
        equal,
        visitor
    );

    return diffs;
}

bool DownloadHandles::
update(QJsonValue const& value)
{
    auto status = toArray(value);
    auto diffs = generateDiff(m_handles, status);

    size_t index = 0;
    bool changed = false;

    for (auto& d: diffs) {
        switch (d.type) {
            using enum diff::type_t;
            case same: {
                auto&& handle = m_handles[index];
                auto&& data = toObject(status[static_cast<int>(d.index)]);
                if (handle->update(data)) {
                    this->handleChanged(index, *handle);
                    changed = true;
                }
                ++index;
                break;
            }
            case insert: {
                auto&& data = toObject(status[static_cast<int>(d.index)]);
                this->insertHandle(index, data);

                ++index;
                changed = true;
                break;
            }
            case remove:
                this->removeHandle(index);
                changed = true;
                break;
            default:
                break;
        }
    }

    return changed;
}

void DownloadHandles::
clear()
{
    m_handles.clear();
}

void DownloadHandles::
refresh()
{
    if (m_aria2.isActive()) {
        doRefresh();
    }
    else {
        m_handles.clear();
    }
}

void DownloadHandles::
insertHandle(size_t const index, QJsonObject const& status)
{
    auto const& gid = getStatusGid(status);

    using diff_t = decltype(m_handles)::difference_type;

    auto const it = m_handles.emplace(
            m_handles.begin() + boost::numeric_cast<diff_t>(index),
            std::make_shared<DownloadHandle>(m_aria2, gid, status));

    Q_EMIT handleCreated(index, **it);
}

void DownloadHandles::
removeHandle(size_t const index)
{
    using diff_t = decltype(m_handles)::difference_type;

    auto const it = m_handles.begin() + boost::numeric_cast<diff_t>(index);
    m_handles.erase(it);

    Q_EMIT handleRemoved(index);
}

void ActiveHandles::
doRefresh()
{
    this->aria2().rpc().tellActive(
        [this](auto const& res) {
            if (res) {
                auto const changed = this->update(res.value());

                if (changed) Q_EMIT this->changed();
            }
            else {
                qDebug() << "ActiveHandles::refresh" << res.error();
            }
        }
    );
}

void WaitingHandles::
doRefresh()
{
    this->aria2().rpc().tellWaiting(0, 1000,
        [this](auto const& res) {
            if (res) {
                auto const changed = this->update(res.value());

                if (changed) Q_EMIT this->changed();
            }
            else {
                qDebug() << "WaitingHandles::refresh" << res.error();
            }
        }
    );
}

void StoppedHandles::
doRefresh()
{
    this->aria2().rpc().tellStopped(0, 1000,
        [this](auto const& res) {
            if (res) {
                auto const changed = this->update(res.value());

                if (changed) Q_EMIT this->changed();
            }
            else {
                qDebug() << "StoppedHandles::refresh" << res.error();
            }
        }
    );
}

} // namespace aria2_remote
