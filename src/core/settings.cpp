#include "settings.hpp"

#include "application.hpp"

#include <QDir>
#include <QString>
#include <QStringList>

namespace aria2_remote {

static auto* const s_keyProfiles = "profiles";

static QString
filePath()
{
    auto const configDir = Application::configDir();
    return configDir.filePath("settings.ini");
}

// Settings

Settings::
Settings()
    : QSettings { filePath(), QSettings::IniFormat }
{}

Settings::~Settings() = default;

QStringList Settings::
profiles() const
{
    return this->value(s_keyProfiles).toStringList();
}

void Settings::
setProfiles(QStringList const& profiles)
{
    this->setValue(s_keyProfiles, profiles);
}

void Settings::
beginProfile(QString const& name)
{
    static QString const fmt { "Profile_%1" };

    this->beginGroup(fmt.arg(name));
}

void Settings::
endProfile()
{
    this->endGroup();
}

} // namespace aria2_remote
