#ifndef ARIA2_REMOTE_CORE_FWD_APPLICATION_HPP
#define ARIA2_REMOTE_CORE_FWD_APPLICATION_HPP

namespace aria2_remote {

class Application;

} // namespace aria2_remote

#endif // ARIA2_REMOTE_CORE_FWD_APPLICATION_HPP
