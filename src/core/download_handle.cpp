#include "download_handle.hpp"

#include "aria2_remote.hpp"
#include "bt_meta_info_data.hpp"
#include "download_handles.hpp"
#include "json_rpc/response.hpp"
#include "utility/json.hpp"
#include "rpc/aria2_rpc.hpp"

#include <cassert>

#include <boost/numeric/conversion/cast.hpp>
#include <boost/range/algorithm/equal.hpp>

#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>

#include <QDebug>
#include <QDebugStateSaver>

namespace aria2_remote {

using util::toArray;
using util::toInt32;
using util::toInt64;
using util::toObject;
using util::toString;
using util::convert;

template<typename T>
static bool
updateValue(T const& src, T& dest)
{
    if (src != dest) {
        dest = src;
        return true;
    }
    return false;
}

template<typename T>
bool
updateValue(QJsonValue const& json, T& dest)
{
    auto const& src = convert<T>(json);

    return updateValue(src, dest);
}

template<typename T>
bool
updateValue(QJsonValue const& json, T& dest, T const& defaultValue)
{
    if (json.isUndefined()) {
        return updateValue(defaultValue, dest);
    }
    else {
        return updateValue(json, dest);
    }
}

template<typename T>
bool
updateValue(QJsonValue const& json, std::optional<T>& optDest)
{
    if (json.isUndefined()) {
        if (optDest) {
            optDest = std::nullopt;
            return true;
        }
    }
    else {
        auto const& src = convert<T>(json);

        if (src != optDest) {
            optDest = src;
            return true;
        }
    }
    return false;
}

// DownloadHandle

DownloadHandle::
DownloadHandle(Aria2Remote& aria2, QString const& gid, QJsonObject const& obj)
    : m_aria2 { aria2 }
    , m_gid { gid }
    , m_uris { std::make_unique<DownloadUris>(*this) }
    , m_servers { aria2.rpc(), gid }
{
    this->connect(m_uris.get(), &DownloadUris::changed,
                  this,         &DownloadHandle::urisChanged);

    update(obj);

    assert(m_status.isValid());
}

DownloadHandle::~DownloadHandle() = default;

QString const* DownloadHandle::
infoHash() const
{
    return m_infoHash.isEmpty() ? nullptr : &m_infoHash;
}

std::vector<QString> const* DownloadHandle::
followedBy() const
{
    return m_followedBy.empty() ? nullptr : &m_followedBy;
}

QString const* DownloadHandle::
following() const
{
    return m_following.isEmpty() ? nullptr : &m_following;
}

QString const* DownloadHandle::
belongsTo() const
{
    return m_belongsTo.isEmpty() ? nullptr : &m_belongsTo;
}

BtMetaInfoData const* DownloadHandle::
bitTorrent() const
{
    return m_bitTorrent.get();
}

QString DownloadHandle::
uri() const
{
    auto const& files = this->files();
    if (files.empty()) return {};

    auto const& uris = files.front().uris();
    if (uris.empty()) {
        return {};
    }
    else {
        return uris.front().uri();
    }
}

ServerDatas const& DownloadHandle::
servers(int32_t const fileIndex) const
{
    return m_servers.servers(fileIndex);
}

void DownloadHandle::
pause()
{
    assert(m_status == DownloadStatus::Active ||
           m_status == DownloadStatus::Waiting);

    bool const wasActive = m_status == DownloadStatus::Active;

    auto& rpc = m_aria2.rpc();
    rpc.pause(m_gid, [this, wasActive, self = this->shared_from_this()]
        (auto const& res) {
            if (res) {
                auto const& gid = toString(res.value());
                assert(gid == m_gid);

                if (wasActive) {
                    m_aria2.activeDownloads().refresh();
                }
                m_aria2.waitingDownloads().refresh();
            }
            else {
                qDebug() << "Error at DownloadHandle::pause" << res.error();
            }
        }
    );
}

void DownloadHandle::
unpause()
{
    assert(m_status == DownloadStatus::Paused);

    auto& rpc = m_aria2.rpc();
    rpc.unpause(m_gid, [this, self = this->shared_from_this()]
        (auto const& res) {
            if (res) {
                auto const& gid = toString(res.value());
                assert(gid == m_gid);

                m_aria2.waitingDownloads().refresh();
            }
            else {
                qDebug() << "Error at DownloadHandle::unpause" << res.error();
            }
        }
    );
}

void DownloadHandle::
remove()
{
    auto& rpc = m_aria2.rpc();

    if (m_status == DownloadStatus::Active ||
        m_status == DownloadStatus::Waiting ||
        m_status == DownloadStatus::Paused)
    {
        rpc.remove(m_gid, [this, self = this->shared_from_this()]
            (auto const& res) {
                if (res) {
                    auto const& gid = toString(res.value());
                    assert(gid == m_gid);

                    m_aria2.refresh();
                }
                else {
                    qDebug() << "Error at DownloadHandle::remove" << res.error();
                }
            }
        );
    }
    else if (m_status == DownloadStatus::Complete ||
             m_status == DownloadStatus::Error ||
             m_status == DownloadStatus::Removed)
    {
        rpc.removeDownloadResult(m_gid,
            [this, self = this->shared_from_this()]
            (auto const& res) {
                if (res) {
                    auto const& result = toString(res.value());
                    assert(result == "OK");

                    m_aria2.refresh();
                }
                else {
                    qDebug() << "Error at DownloadHandle::remove" << res.error();
                }
            }
        );
    }
    else {
        assert(false);
    }
}

void DownloadHandle::
changePosition(int32_t const pos, OffsetMode const how)
{
    assert(m_status == DownloadStatus::Waiting ||
           m_status == DownloadStatus::Paused);

    auto& rpc = m_aria2.rpc();

    rpc.changePosition(m_gid, pos, how,
        [this, self = this->shared_from_this()]
        (auto const& res) {
            if (res) {
                assert(res.value().isDouble());
                m_aria2.waitingDownloads().refresh();
            }
            else {
                qDebug() << "Error at DownloadHandle::changePosition" << res.error();
            }
        }
    );
}

bool DownloadHandle::
update(QJsonObject const& obj)
{
    bool changed = false;

    //qDebug() << obj;

    bool const statusChanged = m_status.update(obj["status"]);
    if (statusChanged) {
        Q_EMIT this->statusChanged();
    }
    changed |= statusChanged;

    changed |= updateValue(obj["totalLength"], m_totalLength);
    changed |= updateValue(obj["completedLength"], m_completedLength);
    changed |= updateValue(obj["uploadLength"], m_uploadLength);

    changed |= updateValue(obj["downloadSpeed"], m_downloadSpeed);
    changed |= updateValue(obj["uploadSpeed"], m_uploadSpeed);

    changed |= updateValue(obj["infoHash"], m_infoHash, QString());
    changed |= updateValue(obj["numSeeders"], m_numSeeders);
    changed |= updateValue(obj["seeder"], m_seeder);

    changed |= updateValue(obj["pieceLength"], m_pieceLength);
    changed |= updateValue(obj["numPieces"], m_numPieces);
    changed |= setBitField(obj["bitfield"]);

    changed |= updateValue(obj["connections"], m_connections);

    changed |= updateValue(obj["errorCode"], m_errorCode, 0);
    changed |= updateValue(obj["errorMessage"], m_errorMessage, QString());

    changed |= setFollowedBy(obj["followedBy"]);
    changed |= updateValue(obj["following"], m_following, QString());
    changed |= updateValue(obj["belongsTo"], m_belongsTo, QString());

    changed |= updateValue(toString(obj["dir"]), m_dir);

    changed |= setFiles(obj["files"]);

    changed |= setBitTorrent(obj["bittorrent"]);

    changed |= updateValue(obj["verifiedLength"], m_verifiedLength);
    changed |= updateValue(obj["verifyIntegrityPending"], m_verifyIntegrityPending);

    if (changed) {
        Q_EMIT this->changed();

        if (m_updateUris) {
            m_uris->update();
        }
    }

    return changed;
}

void DownloadHandle::
setUpdateUris(bool const flag)
{
    if (m_updateUris == flag) return;

    m_updateUris = flag;

    if (flag) {
        m_uris->update();
    }
}

void DownloadHandle::
setUpdateServers(bool const flag)
{
    if (m_updateServers == flag) return;

    m_updateServers = flag;

    if (flag) {
        m_servers.update();
    }
}

bool DownloadHandle::
setFiles(QJsonValue const& value)
{
    assert(value.isArray());
    auto const& files = value.toArray();

    bool changed = false;

    if (boost::numeric_cast<size_t>(files.size()) == m_files.size()) {
        for (qsizetype idx = 0, len = files.size(); idx < len; ++idx) {
            auto const& file = files[idx];
            assert(file.isObject());

            auto& file2 = m_files[static_cast<size_t>(idx)];
            changed |= file2.update(file.toObject());
        }
    }
    else {
        m_files.clear();
        for (auto const& file: files) {
            assert(file.isObject());

            m_files.emplace_back(file.toObject());
        }
        changed = true;
    }

    return changed;
}

bool DownloadHandle::
setBitField(QJsonValue const& value)
{
    if (value.isUndefined()) return false;

    auto const& chars = toString(value);
    BitField bitField { chars, m_numPieces };

    if (bitField == m_bitField) {
        return false;
    }
    else {
        m_bitField = std::move(bitField);
        return true;
    }
}

bool DownloadHandle::
setFollowedBy(QJsonValue const& value)
{
    if (value.isUndefined()) {
        if (m_followedBy.empty()) {
            return false;
        }
        else {
            m_followedBy.clear();
            return true;
        }
    }
    else {
        std::vector<QString> newIds;

        for (auto const& id: toArray(value)) {
            newIds.push_back(toString(id));
        }

        if (!boost::range::equal(newIds, m_followedBy)) {
            m_followedBy = std::move(newIds);
            return true;
        }

        return false;
    }
}

bool DownloadHandle::
setBitTorrent(QJsonValue const& value)
{
    if (value.isUndefined()) {
        if (!m_bitTorrent) {
            return false;
        }
        else {
            m_bitTorrent.reset();
            return true;
        }
    }
    else {
        if (m_bitTorrent) {
            return m_bitTorrent->update(toObject(value));
        }
        else {
            m_bitTorrent = std::make_unique<BtMetaInfoData>(toObject(value));
            return true;
        }
    }
}

QDebug
operator<<(QDebug dbg, DownloadHandle const& handle)
{
    static QString fmt {
        "GID: %1\n"
        "Progress: %2/%3\n"
        "Download Speed: %4\n"
    };

    dbg.noquote().nospace()
        << fmt.arg(handle.m_gid)
              .arg(handle.m_completedLength)
              .arg(handle.m_totalLength)
              .arg(handle.m_downloadSpeed);

    for (auto const& file: handle.m_files) {
        dbg << file << "\n";
    }

    return dbg;
}

} // namespace aria2_remote
