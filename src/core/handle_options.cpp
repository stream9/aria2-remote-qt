#include "handle_options.hpp"

#include "aria2_remote.hpp"
#include "rpc/aria2_rpc.hpp"
#include "download_handle.hpp"
#include "json_rpc/response.hpp"
#include "utility/json.hpp"

namespace aria2_remote {

using util::toString;

HandleOptions::
HandleOptions(DownloadHandle& handle)
    : m_handle { handle.shared_from_this() }
{
    this->refresh();
}

void HandleOptions::
changeOption(QJsonObject const& options)
{
    auto& aria2 = m_handle->aria2();

    aria2.rpc().changeOption(m_handle->gid(), options,
        [this](auto const& res) {
            assert(res); (void)res;
            assert(toString(res.value()) == "OK");

            this->refresh();
        }
    );
}

void HandleOptions::
getOption(Callback const& callback)
{
    auto& aria2 = m_handle->aria2();

    aria2.rpc().getOption(m_handle->gid(), callback);
}

} // namespace aria2_remote
