#include "options.hpp"

#include "utility/json.hpp"
#include "json_rpc/response.hpp"

#include <QDebug>
#include <QDebugStateSaver>
#include <QJsonObject>

namespace aria2_remote {

using util::toString;
using util::toObject;

QString const* Options::
value(QString const& name) const
{
    auto const it = m_options.find(name);
    if (it != m_options.end()) {
        return &it->second;
    }
    else {
        return nullptr;
    }
}

void Options::
setValue(QString const& name, QString const& value)
{
    QJsonObject options;
    options.insert(name, value);

    setValues(options);
}

void Options::
setValues(QJsonObject const& options)
{
    changeOption(options);
}

void Options::
refresh()
{
    getOption(
        [this](auto const& res) {
            assert(res);

            auto const changed = this->update(toObject(res.value()));
            if (changed) {
                Q_EMIT this->changed();
            }

            Q_EMIT this->updated();
        }
    );
}

template<typename Map>
static bool
updateOrInsert(Map& options, QString const& name, QString const& value)
{
    bool changed = false;

    auto it = options.find(name);
    if (it != options.end()) {
        auto& oldValue = it->second;

        if (oldValue != value) {
            oldValue = value;
            changed = true;
        }
    }
    else {
        options.emplace(name, value);
        changed = true;
    }

    return changed;
}

bool Options::
update(QJsonObject const& obj)
{
    auto changed = false;

    for (auto it = obj.begin(); it != obj.end(); ++it) {
        auto const& name = it.key();
        auto const& value = toString(it.value());

        changed |= updateOrInsert(m_options, name, value);
    }

    return changed;
}

QJsonObject Options::
toJson() const
{
    QJsonObject result;

    for (auto const& [name, value]: m_options) {
        result.insert(name, value);
    }

    return result;
}

QDebug
operator<<(QDebug dbg, Options const& options)
{
    static QString const fmt { "%1: %2\n" };
    QDebugStateSaver save { dbg };
    dbg.noquote();
    dbg.nospace();

    for (auto const& [name, value]: options) {
        dbg << fmt.arg(name).arg(value);
    }

    return dbg;
}

} // namespace aria2_remote
