#ifndef ARIA2_REMOTE_ICONS_HPP
#define ARIA2_REMOTE_ICONS_HPP

class QIcon;

namespace aria2_remote::icons {

QIcon quit();
QIcon connect();
QIcon disconnect();
QIcon option();
QIcon handleOption();
QIcon addUri();
QIcon addTorrent();
QIcon addMetalink();
QIcon addServer();
QIcon removeServer();
QIcon editServer();
QIcon moveUp();
QIcon moveDown();
QIcon moveTop() noexcept;
QIcon moveBottom() noexcept;
QIcon pause();
QIcon remove();
QIcon error();
QIcon retry();
QIcon copy();
QIcon remove_completed();

} // namespace aria2_remote::icons

#endif // ARIA2_REMOTE_ICONS_HPP
