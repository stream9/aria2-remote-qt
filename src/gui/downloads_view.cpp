#include "downloads_view.hpp"

#include "action_manager.hpp"
#include "downloads_model.hpp"
#include "header_context_menu.hpp"
#include "name_column_delegate.hpp"

#include "core/application.hpp"
#include "core/aria2_remote.hpp"
#include "core/download_handle.hpp"
#include "gui/dialog/property_dialog.hpp"
#include "utility/pointer.hpp"

#include <cassert>

#include <QItemSelection>
#include <QItemSelectionModel>
#include <QModelIndex>
#include <QPoint>
#include <QContextMenuEvent>

#include <QDebug>

namespace aria2_remote {

static DownloadHandle&
getHandle(QModelIndex const& index)
{
    auto& handle = to_ref(
            static_cast<DownloadHandle*>(index.internalPointer()));

    return handle;
}

static auto
createContextMenu(ActionManager& manager)
{
    auto menu = std::make_unique<QMenu>();
    assert(menu);

    menu->addAction(&manager.pauseAction());
    menu->addAction(&manager.retryAction());
    menu->addSeparator();
    menu->addAction(&manager.copyUriAction());
    menu->addSeparator();
    menu->addAction(&manager.moveToTopAction());
    menu->addAction(&manager.moveUpAction());
    menu->addAction(&manager.moveDownAction());
    menu->addAction(&manager.moveToBottomAction());
    menu->addSeparator();
    menu->addAction(&manager.removeAction());
    menu->addSeparator();
    menu->addAction(&manager.handleOptionAction());
    menu->addAction(&manager.propertyAction());

    return menu;
}

// DownloadsView

DownloadsView::
DownloadsView()
{
    this->setRootIsDecorated(false);
    this->setAlternatingRowColors(true);
    this->setSelectionMode(ExtendedSelection);

    auto delegate = make_qmanaged<NameColumnDelegate>(this);
    this->setItemDelegateForColumn(0, delegate.get());

    this->connect(&gApp(), &Application::aria2Changed,
                  this,    &DownloadsView::createModel);

    this->connect(this, &QAbstractItemView::activated,
                  this, &DownloadsView::onActivated);

    createModel();
    assert(m_model);
}

DownloadsView::~DownloadsView() = default;

void DownloadsView::
createModel()
{
    auto* const aria2 = gApp().aria2();

    if (aria2) {
        m_model = std::make_unique<DownloadsModel>(*aria2);
    }
    else {
        m_model = std::make_unique<DownloadsModel>();
    }

    this->setModel(m_model.get());
    gApp().clearSelection();

    auto const& selectionModel = to_ref(this->selectionModel());

    this->connect(&selectionModel, &QItemSelectionModel::selectionChanged,
                  this,            &DownloadsView::onSelectionChanged);
}

void DownloadsView::
contextMenuEvent(QContextMenuEvent* const ev)
{
    auto& event = to_ref(ev);

    auto menu = createContextMenu(gApp().actionManager());
    menu->exec(event.globalPos());
}

void DownloadsView::
onSelectionChanged(QItemSelection const& selected, QItemSelection const&)
{
    if (selected.isEmpty()) {
        gApp().clearCurrentHandle();
        gApp().clearSelection();
    }
    else {
        auto const& index = selected.front().topLeft();

        auto& handle = getHandle(index);
        gApp().setCurrentHandle(handle);

        auto const& selectionModel = to_ref(this->selectionModel());
        Application::Selection selection;

        for (auto& index: selectionModel.selectedRows()) {
            auto& handle = getHandle(index);

            selection.emplace_back(handle);
        }

        gApp().setSelection(std::move(selection));
    }
}

void DownloadsView::
onActivated(QModelIndex const&)
{
    PropertyDialog dialog;

    dialog.exec();
}

} // namespace aria2_remote
