#ifndef ARIA2_REMOTE_ACTION_MANAGER_HPP
#define ARIA2_REMOTE_ACTION_MANAGER_HPP

#include <memory>

#include <QObject>
#include <QAction>

namespace aria2_remote {

class Application;

class ActionManager : public QObject
{
    Q_OBJECT
public:
    ActionManager(Application&);
    ~ActionManager() override;

    QAction& addUriAction() const { return *m_addUri; }
    QAction& addTorrentAction() const { return *m_addTorrent; }
    QAction& addMetalinkAction() const { return *m_addMetalink; }
    QAction& moveDownAction() const { return *m_moveDown; }
    QAction& moveUpAction() const { return *m_moveUp; }
    QAction& moveToTopAction() const { return *m_moveToTop; }
    QAction& moveToBottomAction() const { return *m_moveToBottom; }
    QAction& optionAction() const { return *m_option; }
    QAction& pauseAction() const { return *m_pause; }
    QAction& quitAction() const { return *m_quit; }
    QAction& removeAction() const { return *m_remove; }
    QAction& handleOptionAction() const { return *m_handleOption; }
    QAction& addServerAction() const { return *m_addServer; }
    QAction& editServerAction() const { return *m_editServer; }
    QAction& removeServerAction() const { return *m_removeServer; }
    QAction& connectAction() const { return *m_connect; }
    QAction& propertyAction() const { return *m_property; }
    QAction& retryAction() const { return *m_retry; }
    QAction& copyUriAction() const { return *m_copyUri; }
    QAction& removeCompletedAction() const { return *m_removeCompleted; }

    Q_SIGNAL void updateRequest() const;

private:
    Application& m_application;

    std::unique_ptr<QAction> m_addUri; // non-null
    std::unique_ptr<QAction> m_addTorrent; // non-null
    std::unique_ptr<QAction> m_addMetalink; // non-null
    std::unique_ptr<QAction> m_moveDown; // non-null
    std::unique_ptr<QAction> m_moveUp; // non-null
    std::unique_ptr<QAction> m_moveToTop; // non-null
    std::unique_ptr<QAction> m_moveToBottom; // non-null
    std::unique_ptr<QAction> m_option; // non-null
    std::unique_ptr<QAction> m_pause; // non-null
    std::unique_ptr<QAction> m_quit; // non-null
    std::unique_ptr<QAction> m_remove; // non-null
    std::unique_ptr<QAction> m_handleOption; // non-null
    std::unique_ptr<QAction> m_addServer; // non-null
    std::unique_ptr<QAction> m_editServer; // non-null
    std::unique_ptr<QAction> m_removeServer; // non-null
    std::unique_ptr<QAction> m_connect; // non-null
    std::unique_ptr<QAction> m_property; // non-null
    std::unique_ptr<QAction> m_retry; // non-null
    std::unique_ptr<QAction> m_copyUri; // non-null
    std::unique_ptr<QAction> m_removeCompleted; // non-null
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_ACTION_MANAGER_HPP
