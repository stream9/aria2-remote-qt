#ifndef ARIA2_REMOTE_OPTION_TOOL_BAR_HPP
#define ARIA2_REMOTE_OPTION_TOOL_BAR_HPP

#include <memory>
#include <optional>

#include "utility/pointer.hpp"

#include <QToolBar>

namespace aria2_remote {

class Application;
class Aria2Remote;
class OptionWidget;
class Server;
class Updater;

class OptionToolBar : public QToolBar
{
    Q_OBJECT
public:
    OptionToolBar(Application&);
    ~OptionToolBar();

    Aria2Remote* aria2() const { return m_aria2; }

    std::optional<int> optionValue(QString const& name) const;
    void setOptionValue(QString const& name, int const value) const;

private:
    Q_SLOT void updateWidgets();
    Q_SLOT void updateAria2();

private:
    Application& m_application;
    Server* m_server = nullptr;
    Aria2Remote* m_aria2 = nullptr;

    qmanaged_ptr<OptionWidget> const m_maxConcurrentDownload; // non-null
    qmanaged_ptr<OptionWidget> const m_maxDownloadSpeed; // non-null
    qmanaged_ptr<OptionWidget> const m_maxUploadSpeed; // non-null
    std::unique_ptr<Updater> m_updater;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_OPTION_TOOL_BAR_HPP
