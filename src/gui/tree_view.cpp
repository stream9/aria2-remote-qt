#include "tree_view.hpp"

#include "header_context_menu.hpp"

#include "core/application.hpp"
#include "core/settings.hpp"
#include "utility/pointer.hpp"

#include <cassert>

#include <QPoint>
#include <QHeaderView>

namespace aria2_remote {

static QString
groupName(QObject const& obj)
{
    auto const& objName = obj.objectName();
    if (!objName.isEmpty()) {
        return objName;
    }
    else {
        auto const& metaObj = to_ref(obj.metaObject());
        return metaObj.className();
    }
}

// TreeView

TreeView::
TreeView(QWidget* const parent/*= nullptr*/)
    : QTreeView { parent }
{
    auto& header = to_ref(this->header());
    header.setContextMenuPolicy(Qt::CustomContextMenu);
    header.setSectionsMovable(true);

    this->connect(&header, &QHeaderView::customContextMenuRequested,
                  this,    &TreeView::onHeaderContextMenuRequested);
}

void TreeView::
readSettings()
{
    auto& header = to_ref(this->header());

    auto& settings = gApp().settings();

    settings.beginGroup(groupName(*this));

    header.restoreState(settings.value("headerState").toByteArray());

    settings.endGroup();
}

void TreeView::
writeSettings() const
{
    auto const& header = to_ref(this->header());

    auto& settings = gApp().settings();

    settings.beginGroup(groupName(*this));

    settings.setValue("headerState", header.saveState());

    settings.endGroup();
}

void TreeView::
onHeaderContextMenuRequested(QPoint const& pos) const
{
    auto& header = to_ref(this->header());
    auto const& model = to_ref(this->model());

    HeaderContextMenu menu { header, model };

    menu.exec(this->mapToGlobal(pos));
}

} // namespace aria2_remote
