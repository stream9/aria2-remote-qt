#include "action_manager.hpp"

#include "action/add_metalink.hpp"
#include "action/add_server.hpp"
#include "action/add_torrent.hpp"
#include "action/add_uri.hpp"
#include "action/connect.hpp"
#include "action/copy_uri.hpp"
#include "action/edit_server.hpp"
#include "action/handle_option.hpp"
#include "action/move_down.hpp"
#include "action/move_to_bottom.hpp"
#include "action/move_to_top.hpp"
#include "action/move_up.hpp"
#include "action/option.hpp"
#include "action/pause.hpp"
#include "action/property.hpp"
#include "action/quit.hpp"
#include "action/remove.hpp"
#include "action/remove_completed.hpp"
#include "action/remove_server.hpp"
#include "action/retry.hpp"
#include "core/application.hpp"
#include "core/aria2_remote.hpp"
#include "core/download_handle.hpp"

namespace aria2_remote {

ActionManager::
ActionManager(Application& app)
    : m_application { app }
    , m_addUri { std::make_unique<action::AddUri>(*this, app) }
    , m_addTorrent { std::make_unique<action::AddTorrent>(*this, app) }
    , m_addMetalink { std::make_unique<action::AddMetalink>(*this, app) }
    , m_moveDown { std::make_unique<action::MoveDown>(*this, app) }
    , m_moveUp { std::make_unique<action::MoveUp>(*this, app) }
    , m_moveToTop { std::make_unique<action::MoveToTop>(*this, app) }
    , m_moveToBottom { std::make_unique<action::MoveToBottom>(*this, app) }
    , m_option { std::make_unique<action::Option>(*this, app) }
    , m_pause { std::make_unique<action::Pause>(*this, app) }
    , m_quit { std::make_unique<action::Quit>(*this) }
    , m_remove { std::make_unique<action::Remove>(*this, app) }
    , m_handleOption { std::make_unique<action::HandleOption>(*this, app) }
    , m_addServer { std::make_unique<action::AddServer>(*this, app.servers()) }
    , m_editServer { std::make_unique<action::EditServer>(*this, app) }
    , m_removeServer { std::make_unique<action::RemoveServer>(*this, app) }
    , m_connect { std::make_unique<action::Connect>(*this, app) }
    , m_property { std::make_unique<action::Property>(*this, app) }
    , m_retry { std::make_unique<action::Retry>(*this, app) }
    , m_copyUri { std::make_unique<action::CopyUri>(*this, app) }
    , m_removeCompleted { std::make_unique<action::RemoveCompleted>(*this, app) }
{
    Q_EMIT updateRequest();

    this->connect(&m_application, &Application::selectionChanged,
                  this,           &ActionManager::updateRequest);
}

ActionManager::~ActionManager() = default;

} // namespace aria2_remote
