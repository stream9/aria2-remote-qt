#ifndef ARIA2_REMOTE_STATUS_BAR_HPP
#define ARIA2_REMOTE_STATUS_BAR_HPP

#include "utility/pointer.hpp"

#include <QStatusBar>

class QLabel;

namespace aria2_remote {

class Aria2Remote;

class StatusBar : public QStatusBar
{
    Q_OBJECT
public:
    StatusBar();

private:
    Q_SLOT void updateAria2();
    Q_SLOT void refresh();

private:
    Aria2Remote* m_aria2 = nullptr;

    qmanaged_ptr<QLabel> m_active; // non-null
    qmanaged_ptr<QLabel> m_waiting; // non-null
    qmanaged_ptr<QLabel> m_stopped; // non-null
    qmanaged_ptr<QLabel> m_speed; // non-null
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_STATUS_BAR_HPP
