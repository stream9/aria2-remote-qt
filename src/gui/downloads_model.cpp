#include "downloads_model.hpp"

#include "core/aria2_remote.hpp"
#include "core/download_handle.hpp"
#include "core/download_handles.hpp"
#include "utility/format.hpp"
#include "utility/valid_model_index.hpp"

#include <cassert>
#include <optional>

#include <boost/numeric/conversion/cast.hpp>

#include <QFileInfo>
#include <QModelIndex>
#include <QVariant>

namespace aria2_remote {

using util::formatTime;
using util::formatBandwidth;
using util::formatSize;

static auto
filename(QString const& path)
{
    auto const idx = path.lastIndexOf("/");

    return idx == -1 ? path : path.mid(idx+1);
}

static QVariant
formatName(DownloadHandle const& handle)
{
    auto const& files = handle.files();
    assert(!files.empty());

    auto const& file = files.front();
    auto const& path = file.path();

    if (path.isEmpty()) {
        auto const& uris = file.uris();
        assert(!uris.empty());

        auto const& uri = uris.front();
        return uri.uri();
    }
    else {
        return filename(path);
    }
}

static QVariant
formatStatus(DownloadHandle const& handle)
{
    return handle.status().text();
}

static QVariant
formatProgress(DownloadHandle const& handle)
{
    static QString fmt { "%1%" };

    auto const completed = handle.completedLength();
    auto const total = handle.totalLength();

    using boost::numeric_cast;
    auto const ratio =
        total == 0 ? 0.0 : (numeric_cast<double>(completed) / numeric_cast<double>(total) * 100);

    return fmt.arg(ratio, 0, 'g', 4);
}

static QVariant
formatProgressLong(DownloadHandle const& handle)
{
    static QString fmt { "%1 / %2" };

    auto const completed = handle.completedLength();
    auto const total = handle.totalLength();

    return fmt.arg(completed).arg(total);
}

static QVariant
formatTotalLength(DownloadHandle const& handle)
{
    return QVariant::fromValue(
        formatSize(boost::numeric_cast<uint64_t>(handle.totalLength())));
}

static QVariant
formatCompletedLength(DownloadHandle const& handle)
{
    return QVariant::fromValue(
       formatSize(boost::numeric_cast<uint64_t>(handle.completedLength())));
}

static QVariant
formatEta(DownloadHandle const& handle)
{
    auto const remainLength = handle.totalLength() - handle.completedLength();
    if (remainLength == 0) return {};
    assert(remainLength > 0);

    auto const bytePerSec = handle.downloadSpeed();
    if (bytePerSec == 0) return {};
    assert(bytePerSec > 0);

    auto const remainSec = remainLength / bytePerSec;

    return formatTime(boost::numeric_cast<uint64_t>(remainSec));
}

static QVariant
formatConnections(DownloadHandle const& handle)
{
    return QVariant::fromValue(handle.connections());
}

static QVariant
formatSpeed(DownloadHandle const& handle)
{
    auto const speed = handle.downloadSpeed();
    if (speed == 0) return {};

    assert(speed > 0);

    return QVariant::fromValue(
        formatBandwidth(boost::numeric_cast<uint64_t>(handle.downloadSpeed())));
}

static QVariant
formatPieceLength(DownloadHandle const& handle)
{
    return QVariant::fromValue(handle.pieceLength());
}

static QVariant
formatNumPieces(DownloadHandle const& handle)
{
    return QVariant::fromValue(handle.numPieces());
}

// DownloadsModel

DownloadsModel::
DownloadsModel(Aria2Remote& aria2)
    : m_aria2 { aria2.shared_from_this() }
{
    auto const& actives = m_aria2->activeDownloads();
    this->connect(&actives, &DownloadHandles::handleCreated,
                  this,     &DownloadsModel::onActiveDownloadCreated);
    this->connect(&actives, &DownloadHandles::handleChanged,
                  this,     &DownloadsModel::onActiveDownloadChanged);
    this->connect(&actives, &DownloadHandles::handleRemoved,
                  this,     &DownloadsModel::onActiveDownloadRemoved);
    this->connect(&actives, &DownloadHandles::handleMoved,
                  this,     &DownloadsModel::onActiveDownloadMoved);

    auto const& waitings = m_aria2->waitingDownloads();
    this->connect(&waitings, &DownloadHandles::handleCreated,
                  this,      &DownloadsModel::onWaitingDownloadCreated);
    this->connect(&waitings, &DownloadHandles::handleChanged,
                  this,      &DownloadsModel::onWaitingDownloadChanged);
    this->connect(&waitings, &DownloadHandles::handleRemoved,
                  this,      &DownloadsModel::onWaitingDownloadRemoved);
    this->connect(&waitings, &DownloadHandles::handleMoved,
                  this,      &DownloadsModel::onWaitingDownloadMoved);

    auto const& stoppeds = m_aria2->stoppedDownloads();
    this->connect(&stoppeds, &DownloadHandles::handleCreated,
                  this,      &DownloadsModel::onStoppedDownloadCreated);
    this->connect(&stoppeds, &DownloadHandles::handleChanged,
                  this,      &DownloadsModel::onStoppedDownloadCreated);
    this->connect(&stoppeds, &DownloadHandles::handleRemoved,
                  this,      &DownloadsModel::onStoppedDownloadRemoved);
    this->connect(&stoppeds, &DownloadHandles::handleMoved,
                  this,      &DownloadsModel::onStoppedDownloadMoved);
}

QModelIndex DownloadsModel::
index(int const row, int const column,
                     QModelIndex const& parent/*= {}*/) const
{
    assert(!parent.isValid()); (void)parent;

    if (column < 0 || column > Column::Count) {
        //qDebug() << __func__ << "irregular column" << column;
        return {};
    }
    if (row < 0 || row >= rowCount()) {
        //qDebug() << __func__ << "irregular row" << row;
        return {};
    }

    auto& handle = getHandle(row);

    return this->createIndex(row, column, &handle);
}

QModelIndex DownloadsModel::
parent(QModelIndex const&) const
{
    return {};
}

int DownloadsModel::
rowCount(QModelIndex const& parent/*= {}*/) const
{
    if (parent.isValid()) return 0;
    if (!m_aria2) return 0;

    return boost::numeric_cast<int>(
        m_aria2->activeDownloads().size()
      + m_aria2->waitingDownloads().size()
      + m_aria2->stoppedDownloads().size() );
}

int DownloadsModel::
columnCount(QModelIndex const&/*parent = {}*/) const
{
    return Column::Count;
}

QVariant DownloadsModel::
data(QModelIndex const& index, int role/*= Qt::DisplayRole*/) const
{
    switch (role) {
    case Qt::DisplayRole:
        return displayData(index);
    case Qt::ToolTipRole:
        return toolTipData(index);
    case Qt::TextAlignmentRole:
        return alignmentData(index);
    default:
        return {};
    }
}

QVariant DownloadsModel::
headerData(int const section,
           Qt::Orientation const orientation,
           int const role/*= Qt::DisplayRole*/) const
{
    if (role != Qt::DisplayRole) return {};
    if (orientation != Qt::Horizontal) return {};

    switch (section) {
    case Name:
        return "Name";
    case Status:
        return "Status";
    case Progress:
        return "Progress";
    case TotalLength:
        return "Toral Length";
    case CompletedLength:
        return "Completed Length";
    case Eta:
        return "ETA";
    case Connections:
        return "Connections";
    case DownloadSpeed:
        return "Download Speed";
    case PieceLength:
        return "Piece Length";
    case NumPieces:
        return "# of Pieces";
    default:
        throw std::logic_error("DownloadModel::headerData");
    }
}

Qt::ItemFlags DownloadsModel::
flags(QModelIndex const& index) const noexcept
{
    if (!index.isValid()) return {};

    Qt::ItemFlags result = Qt::ItemIsEnabled;
    if (index.column() == Name) {
        result |= Qt::ItemIsSelectable;
    }

    return result;
}

DownloadHandle& DownloadsModel::
getHandle(int const r) const
{
    //assert(0 <= row && row < rowCount());
    assert(m_aria2);

    auto&& activeDownloads = m_aria2->activeDownloads();
    auto&& waitingDownloads = m_aria2->waitingDownloads();
    auto&& stoppedDownloads = m_aria2->stoppedDownloads();

    auto row = boost::numeric_cast<size_t>(r);
    if (auto const count = activeDownloads.size(); row < count) {
        return activeDownloads[row];
    }
    else {
        assert(row >= count);
        row -= count;
    }

    if (auto const count = waitingDownloads.size(); row < count) {
        return waitingDownloads[row];
    }
    else {
        assert(row >= count);
        row -= count;
    }

    if (row < stoppedDownloads.size()) {
        return stoppedDownloads[row];
    }

    throw std::logic_error("DownloadsModel::getHandle");
}

DownloadHandle const& DownloadsModel::
getHandle(ConstValidModelIndex const& index) const
{
    auto* const handle =
            static_cast<DownloadHandle const*>(index->internalPointer());
    assert(handle);

    return *handle;
}

template<typename Handles>
static std::optional<size_t>
indexOf(Handles const& handles, DownloadHandle const& target)
{
    auto const it = std::find_if(handles.begin(), handles.end(),
        [&](auto const& handle) {
            return handle.get() == &target;
        }
    );

    if (it != handles.end()) {
        return { std::distance(handles.begin(), it) };
    }
    else {
        return {};
    }
}

int DownloadsModel::
getRow(DownloadHandle& handle) const
{
    assert(m_aria2);
    using boost::numeric_cast;

    auto const& activeDownloads = m_aria2->activeDownloads();

    if (auto const index = indexOf(activeDownloads, handle)) {
        return numeric_cast<int>(index.value());
    }

    auto offset = numeric_cast<int>(activeDownloads.size());

    auto const& waitingDownloads = m_aria2->waitingDownloads();

    if (auto const index = indexOf(waitingDownloads, handle)) {
        return offset + numeric_cast<int>(index.value());
    }

    offset += numeric_cast<int>(waitingDownloads.size());

    if (auto const index = indexOf(m_aria2->stoppedDownloads(), handle)) {
        return offset + numeric_cast<int>(index.value());
    }

    throw std::logic_error("DownloadsModel::getRow");
}

QVariant DownloadsModel::
displayData(ConstValidModelIndex const& index) const
{
    auto const column = index->column();

    auto const& handle = getHandle(index);

    switch (column) {
    case Name:
        return formatName(handle);
    case Status:
        return formatStatus(handle);
    case Progress:
        return formatProgress(handle);
    case TotalLength:
        return formatTotalLength(handle);
    case CompletedLength:
        return formatCompletedLength(handle);
    case Eta:
        return formatEta(handle);
    case Connections:
        return formatConnections(handle);
    case DownloadSpeed:
        return formatSpeed(handle);
    case PieceLength:
        return formatPieceLength(handle);
    case NumPieces:
        return formatNumPieces(handle);
    default:
        throw std::logic_error("DownloadsModel::displayData");
    }
}

QVariant DownloadsModel::
toolTipData(ConstValidModelIndex const& index) const
{
    auto const column = index->column();

    switch (column) {
    case Name:
        return formatName(getHandle(index));
    case Progress:
        return formatProgressLong(getHandle(index));
    default:
        return {};
    }
}

QVariant DownloadsModel::
alignmentData(ConstValidModelIndex const& index) const
{
    auto const column = index->column();

    switch (column) {
    case DownloadsModel::Name:
    case DownloadsModel::Status:
        return {};
    default:
        return Qt::AlignRight;
    }
}

void DownloadsModel::
onDownloadCreated(size_t const index)
{
    auto const row = boost::numeric_cast<int>(index);
    this->beginInsertRows({}, row, row);
    this->endInsertRows();
}

void DownloadsModel::
onDownloadChanged(size_t const index)
{
    auto const row = boost::numeric_cast<int>(index);

    auto const& topLeft = this->index(row, 0);
    auto const& bottomRight = this->index(row, Column::Count-1);

    this->dataChanged(topLeft, bottomRight);
}

void DownloadsModel::
onDownloadRemoved(size_t const index)
{
    auto const row = boost::numeric_cast<int>(index);

    this->beginRemoveRows({}, row, row);
    this->endRemoveRows();
}

void DownloadsModel::
onDownloadMoved(size_t const from, size_t const to)
{
    auto const from_ = boost::numeric_cast<int>(from);
    auto const to_= boost::numeric_cast<int>(to);

    this->beginMoveRows({}, from_, from_, {}, to_);
    this->endMoveRows();
}

void DownloadsModel::
onActiveDownloadCreated(size_t const index, DownloadHandle&)
{
    onDownloadCreated(index);
}

void DownloadsModel::
onActiveDownloadChanged(size_t const index, DownloadHandle&)
{
    onDownloadChanged(index);
}

void DownloadsModel::
onActiveDownloadRemoved(size_t const index)
{
    onDownloadRemoved(index);
}

void DownloadsModel::
onActiveDownloadMoved(size_t const from, size_t const to, DownloadHandle&)
{
    onDownloadMoved(from, to);
}

void DownloadsModel::
onWaitingDownloadCreated(size_t const index, DownloadHandle&)
{
    assert(m_aria2);

    auto const offset = m_aria2->activeDownloads().size();
    onDownloadCreated(offset + index);
}

void DownloadsModel::
onWaitingDownloadChanged(size_t const index, DownloadHandle&)
{
    assert(m_aria2);

    auto const offset = m_aria2->activeDownloads().size();
    onDownloadChanged(offset + index);
}

void DownloadsModel::
onWaitingDownloadRemoved(size_t const index)
{
    assert(m_aria2);

    auto const offset = m_aria2->activeDownloads().size();
    onDownloadRemoved(offset + index);
}

void DownloadsModel::
onWaitingDownloadMoved(size_t const from, size_t const to, DownloadHandle&)
{
    assert(m_aria2);

    auto const offset = m_aria2->activeDownloads().size();
    onDownloadMoved(offset + from, offset + to);
}

void DownloadsModel::
onStoppedDownloadCreated(size_t const index, DownloadHandle&)
{
    assert(m_aria2);

    auto const offset = m_aria2->activeDownloads().size()
                      + m_aria2->waitingDownloads().size();
    onDownloadCreated(offset + index);
}

void DownloadsModel::
onStoppedDownloadChanged(size_t const index, DownloadHandle&)
{
    assert(m_aria2);

    auto const offset = m_aria2->activeDownloads().size()
                      + m_aria2->waitingDownloads().size();
    onDownloadChanged(offset + index);
}

void DownloadsModel::
onStoppedDownloadRemoved(size_t const index)
{
    assert(m_aria2);

    auto const offset = m_aria2->activeDownloads().size()
                      + m_aria2->waitingDownloads().size();
    onDownloadRemoved(offset + index);
}

void DownloadsModel::
onStoppedDownloadMoved(size_t const from, size_t const to, DownloadHandle&)
{
    assert(m_aria2);

    auto const offset = m_aria2->activeDownloads().size()
                      + m_aria2->waitingDownloads().size();
    onDownloadMoved(offset + from, offset + to);
}

} // namespace aria2_remote
