#include "icons.hpp"

#include <QIcon>

namespace aria2_remote::icons {

QIcon
quit()
{
    return QIcon::fromTheme("application-exit");
}

QIcon
connect()
{
    return QIcon::fromTheme("network-connect");
}

QIcon
disconnect()
{
    return QIcon::fromTheme("network-disconnect");
}

QIcon
option()
{
    return QIcon::fromTheme("document-properties");
}

QIcon
handleOption()
{
    return QIcon::fromTheme("document-properties");
}

QIcon
addUri()
{
    return QIcon::fromTheme("list-add");
}

QIcon
addTorrent()
{
    return QIcon::fromTheme("list-add");
}

QIcon
addMetalink()
{
    return QIcon::fromTheme("list-add");
}

QIcon
addServer()
{
    return QIcon::fromTheme("network-server");
}

QIcon
removeServer()
{
    return QIcon::fromTheme("network-server");
}

QIcon
editServer()
{
    return QIcon::fromTheme("network-server");
}

QIcon
moveUp()
{
    return QIcon::fromTheme("go-up");
}

QIcon
moveDown()
{
    return QIcon::fromTheme("go-down");
}

QIcon
moveTop() noexcept
{
    return QIcon::fromTheme("go-top");
}

QIcon
moveBottom() noexcept
{
    return QIcon::fromTheme("go-bottom");
}

QIcon
pause()
{
    return QIcon::fromTheme("media-playback-pause");
}

QIcon
remove()
{
    return QIcon::fromTheme("edit-delete");
}

QIcon
error()
{
    return QIcon(":/salmon-error-16.png");
}

QIcon
retry()
{
    return QIcon::fromTheme("edit-redo");
}

QIcon
copy()
{
    return QIcon::fromTheme("edit-copy");
}

QIcon
remove_completed()
{
    return QIcon::fromTheme("checkmark");
}

} // namespace aria2_remote::icons
