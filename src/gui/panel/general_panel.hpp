#ifndef ARIA2_REMOTE_GENERAL_PANEL_HPP
#define ARIA2_REMOTE_GENERAL_PANEL_HPP

#include "utility/pointer.hpp"

#include <QWidget>

class QStackedLayout;

namespace aria2_remote {

class DownloadHandle;
class TorrentGeneralPanel;
class UriGeneralPanel;

class GeneralPanel : public QWidget
{
    Q_OBJECT
public:
    GeneralPanel();

private:
    Q_SLOT void selectPanel(DownloadHandle*);

private:
    qmanaged_ptr<QStackedLayout> m_stack;
    qmanaged_ptr<UriGeneralPanel> m_uriPanel;
    qmanaged_ptr<TorrentGeneralPanel> m_torrentPanel;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_GENERAL_PANEL_HPP
