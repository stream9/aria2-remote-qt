#include "files_panel.hpp"

#include "file_servers_model.hpp"
#include "file_uris_model.hpp"
#include "files_model.hpp"

#include "core/application.hpp"
#include "core/download_handle.hpp"
#include "core/settings.hpp"
#include "gui/tree_view.hpp"

#include <cassert>
#include <memory>

#include <QItemSelectionModel>
#include <QClipboard>
#include <QContextMenuEvent>
#include <QHideEvent>
#include <QShowEvent>
#include <QAction>
#include <QApplication>
#include <QBoxLayout>
#include <QHeaderView>
#include <QMenu>
#include <QSplitter>
#include <QTabWidget>

#include <QDebug>

namespace aria2_remote {

// FilesView

class FilesPanel::FilesView: public TreeView
{
    Q_OBJECT
public:
    FilesView(QWidget& parent);

    void setHandle(DownloadHandle&);
    void resetHandle();

private:
    std::unique_ptr<FilesModel> m_model;
};

FilesPanel::FilesView::
FilesView(QWidget& parent)
    : TreeView { &parent }
    , m_model { std::make_unique<FilesModel>() }
{
    assert(m_model);

    this->setModel(m_model.get());
}

void FilesPanel::FilesView::
setHandle(DownloadHandle& handle)
{
    m_model->setHandle(handle);

    this->setCurrentIndex(m_model->index(0, 0));
}

void FilesPanel::FilesView::
resetHandle()
{
    m_model->resetHandle();

    this->setCurrentIndex({});
}

// ServersView

class FilesPanel::ServersView: public TreeView
{
    Q_OBJECT
public:
    ServersView(QWidget& parent);

    void setFile(DownloadHandle&, FileData&);
    void resetFile();

    void showEvent(QShowEvent* const event) override
    {
        assert(event);

        if (event->spontaneous() && m_handle) {
            m_handle->setUpdateServers(true);
        }

        QTreeView::showEvent(event);
    }

    void hideEvent(QHideEvent* const event) override
    {
        assert(event);

        if (event->spontaneous() && m_handle) {
            m_handle->setUpdateServers(true);
        }

        QTreeView::hideEvent(event);
    }

private:
    std::unique_ptr<QAbstractItemModel> m_model;
    DownloadHandle* m_handle = nullptr;
};

FilesPanel::ServersView::
ServersView(QWidget& parent)
    : TreeView { &parent }
{
    resetFile();
    assert(m_model);
}

void FilesPanel::ServersView::
setFile(DownloadHandle& handle, FileData& fileData)
{
    m_handle = &handle;

    m_model = std::make_unique<FileServersModel>(handle, fileData);
    assert(m_model);

    this->setModel(m_model.get());
}

void FilesPanel::ServersView::
resetFile()
{
    m_handle = nullptr;

    m_model = std::make_unique<FileServersModel>();
    assert(m_model);

    this->setModel(m_model.get());
}

// UrisView

class FilesPanel::UrisView: public TreeView
{
    Q_OBJECT
public:
    UrisView(QWidget& parent);

    void setFile(DownloadHandle&, FileData&);
    void resetFile();

private:
    void contextMenuEvent(QContextMenuEvent*) override;

    Q_SLOT void copyUri();

private:
    std::unique_ptr<QAbstractItemModel> m_model;
    QAction m_copyAction;
    QModelIndex m_contextMenuIndex;
};

FilesPanel::UrisView::
UrisView(QWidget& parent)
    : TreeView { &parent }
    , m_copyAction { "&Copy URI" }
{
    resetFile();
    assert(m_model);

    m_copyAction.setEnabled(false);
    this->connect(&m_copyAction, &QAction::triggered,
                  this,          &UrisView::copyUri);

    this->addAction(&m_copyAction);
}

void FilesPanel::UrisView::
setFile(DownloadHandle& handle, FileData& fileData)
{
    m_model = std::make_unique<FileUrisModel>(handle, fileData);
    assert(m_model);

    this->setModel(m_model.get());
}

void FilesPanel::UrisView::
resetFile()
{
    m_model = std::make_unique<FileUrisModel>();
    assert(m_model);

    this->setModel(m_model.get());
}

void FilesPanel::UrisView::
contextMenuEvent(QContextMenuEvent* const event)
{
    assert(event);

    m_contextMenuIndex = this->indexAt(event->pos());

    m_copyAction.setEnabled(m_contextMenuIndex.isValid());

    QMenu menu;
    menu.addAction(&m_copyAction);

    menu.exec(event->globalPos());

    m_contextMenuIndex = {};
}

void FilesPanel::UrisView::
copyUri()
{
    auto const& uriIndex =
            m_model->index(m_contextMenuIndex.row(), UrisModel::URI);
    assert(uriIndex.isValid());

    auto const& uri = uriIndex.data().toString();

    auto& clipboard = to_ref(QApplication::clipboard());
    clipboard.setText(uri);
}

// FilesPanel

FilesPanel::
FilesPanel()
    : m_splitter { make_qmanaged<QSplitter>(Qt::Vertical, this) }
    , m_tab { make_qmanaged<QTabWidget>(this) }
    , m_files { make_qmanaged<FilesView>(*this) }
    , m_uris { make_qmanaged<UrisView>(*this) }
    , m_servers { make_qmanaged<ServersView>(*this) }
{
    auto layout = make_qmanaged<QVBoxLayout>(this);

    layout->addWidget(m_splitter.get());

    auto& selectionModel = to_ref(m_files->selectionModel());

    this->connect(&selectionModel, &QItemSelectionModel::currentChanged,
                  this,            &FilesPanel::setCurrentFile);

    m_splitter->addWidget(m_files.get());

    m_tab->setTabPosition(QTabWidget::South);

    m_splitter->addWidget(m_tab.get());

    m_tab->addTab(m_uris.get(), "URIs");

    m_tab->addTab(m_servers.get(), "Servers");

    this->connect(&gApp(), &Application::currentHandleChanged,
                  this,         &FilesPanel::setDownloadHandle);

    setDownloadHandle(gApp().currentHandle());
}

FilesPanel::~FilesPanel() = default;

void FilesPanel::
readSettings()
{
    auto& settings = gApp().settings();

    settings.beginGroup("FilePage");

    m_splitter->restoreState(settings.value("splitter").toByteArray());
    m_tab->setCurrentIndex(settings.value("tab").toInt());

    m_files->readSettings();
    m_uris->readSettings();
    m_servers->readSettings();

    settings.endGroup();
}

void FilesPanel::
writeSettings()
{
    auto& settings = gApp().settings();

    settings.beginGroup("FilePage");

    settings.setValue("splitter", m_splitter->saveState());
    settings.setValue("tab", m_tab->currentIndex());

    m_files->writeSettings();
    m_uris->writeSettings();
    m_servers->writeSettings();

    settings.endGroup();
}

void FilesPanel::
setDownloadHandle(DownloadHandle* const handle)
{
    if (handle == m_handle) return;

    m_handle = handle;

    if (handle) {
        m_files->setHandle(*handle);
    }
    else {
        m_files->resetHandle();
    }
}

void FilesPanel::
setCurrentFile(QModelIndex const& index)
{
    if (!m_handle) return;

    if (index.isValid()) {
        auto* const fileData = static_cast<FileData*>(index.internalPointer());
        assert(fileData);

        m_uris->setFile(*m_handle, *fileData);
        m_servers->setFile(*m_handle, *fileData);
    }
    else {
        m_uris->resetFile();
        m_servers->resetFile();
    }
}

#include "files_panel.moc"

} // namespace aria2_remote
