#include "uris_model.hpp"

#include "core/download_handle.hpp"

#include <cassert>

#include <boost/numeric/conversion/cast.hpp>

#include <QDebug>
#include <QModelIndex>
#include <QVariant>

namespace aria2_remote {

static QVariant
getStatus(UriData const& uri)
{
    return uri.statusText();
}

static QVariant
getUri(UriData const& uri)
{
    return uri.uri();
}

// UrisModel

UrisModel::
UrisModel(DownloadHandle* const handle)
{
    if (handle) {
        setHandle(*handle);
    }
}

void UrisModel::
setHandle(DownloadHandle& handle)
{
    if (&handle == m_handle) return;

    disconnectFromHandle();

    m_handle = &handle;
    assert(m_handle);

    connectToHandle();

    onDownloadHandleChanged();
}

void UrisModel::
resetHandle()
{
    disconnectFromHandle();

    m_handle = nullptr;

    onDownloadHandleChanged();
}

QModelIndex UrisModel::
index(int const row, int const column,
                     QModelIndex const& parent/*= {}*/) const
{
    assert(row >= 0);
    assert(column >= 0);
    assert(column < Column::Count);
    assert(!parent.isValid()); (void)parent;

    auto const& uris = this->uris();
    assert(boost::numeric_cast<size_t>(row) < uris.size());

    auto const& uriData = uris[static_cast<size_t>(row)];

    return this->createIndex(row, column, const_cast<UriData*>(&uriData));
}

QModelIndex UrisModel::
parent(QModelIndex const&) const
{
    return {};
}

int UrisModel::
rowCount(QModelIndex const& parent/*= {}*/) const
{
    if (parent.isValid()) return 0;

    auto const& uris = this->uris();

    return boost::numeric_cast<int>(uris.size());
}

int UrisModel::
columnCount(QModelIndex const& parent/*= {}*/) const
{
    assert(!parent.isValid()); (void)parent;

    return Column::Count;
}

QVariant UrisModel::
data(QModelIndex const& index, int const role/*= Qt::DisplayRole*/) const
{
    if (role == Qt::ToolTipRole) return toolTipData(index);
    else if (role != Qt::DisplayRole) return {};

    assert(index.isValid());

    auto* const uriData = static_cast<UriData const*>(index.internalPointer());
    assert(uriData);

    auto const column = index.column();

    switch (column) {
    case Status:
        return getStatus(*uriData);
    case URI:
        return getUri(*uriData);
    default:
        qDebug() << "UrisModel::data() unknown column: " << column;
        return {};
    }
}

QVariant UrisModel::
headerData(int const section, Qt::Orientation const orientation,
                              int const role/*= Qt::DisplayRole*/) const
{
    if (role != Qt::DisplayRole) return {};

    assert(orientation == Qt::Horizontal); (void)orientation;

    switch (section) {
    case Status:
        return "Status";
    case URI:
        return "URI";
    default:
        qDebug() << "UrisModel::headerData() unknown section: " << section;
        return {};
    }
}

QVariant UrisModel::
toolTipData(QModelIndex const& index) const
{
    assert(index.isValid());

    auto* const uriData = static_cast<UriData const*>(index.internalPointer());
    assert(uriData);

    auto const column = index.column();
    if (column == URI) {
        return getUri(*uriData);
    }

    return {};
}

void UrisModel::
connectToHandle()
{
    assert(m_handle);

    this->connect(m_handle,  &DownloadHandle::changed,
                  this,      &UrisModel::onDownloadHandleChanged);

    this->connect(m_handle, &QObject::destroyed,
                  this,     &UrisModel::resetHandle);
}

void UrisModel::
disconnectFromHandle()
{
    if (!m_handle) return;

    this->disconnect(m_handle,  &DownloadHandle::changed,
                     this,      &UrisModel::onDownloadHandleChanged);

    this->disconnect(m_handle, &QObject::destroyed,
                     this,     &UrisModel::resetHandle);
}

void UrisModel::
onDownloadHandleChanged()
{
    this->endResetModel();
}

} // namespace aria2_remote

