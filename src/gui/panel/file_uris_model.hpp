#ifndef ARIA2_REMOTE_FILE_URIS_MODEL_HPP
#define ARIA2_REMOTE_FILE_URIS_MODEL_HPP

#include "uris_model.hpp"

#include <vector>

namespace aria2_remote {

class DownloadHandle;
class FileData;

class FileUrisModel : public UrisModel
{
    Q_OBJECT
public:
    FileUrisModel();
    FileUrisModel(DownloadHandle&, FileData const&);

private:
    UriDatas const& uris() const override;

    Q_SLOT void resetFile();

private:
    FileData const* m_fileData = nullptr;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_FILE_URIS_MODEL_HPP
