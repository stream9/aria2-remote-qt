#include "file_servers_model.hpp"

#include "core/download_handle.hpp"
#include "core/file_data.hpp"
#include "core/server_data.hpp"

#include <cassert>

#include <boost/numeric/conversion/cast.hpp>

#include <QModelIndex>
#include <QVariant>

namespace aria2_remote {

static QVariant
getUri(ServerData const& server)
{
    return server.uri();
}

static QVariant
getCurrentUri(ServerData const& server)
{
    return server.currentUri();
}

static QVariant
getDownloadSpeed(ServerData const& server)
{
    return QVariant::fromValue(server.downloadSpeed());
}

// FileServersModel

FileServersModel::
FileServersModel(DownloadHandle const& handle, FileData const& fileData)
    : m_handle { &handle }
    , m_fileData { &fileData }
{
    this->connect(m_handle,  &DownloadHandle::serversChanged,
                  this,      &FileServersModel::onDataChanged);
}

QModelIndex FileServersModel::
index(int const row, int const column,
                     QModelIndex const& parent/*= {}*/) const
{
    assert(row >= 0);
    assert(column >= 0);
    assert(column < Column::Count);
    assert(!parent.isValid()); (void)parent;

    auto const servers = this->servers();

    assert(static_cast<size_t>(row) < servers.size());

    auto const& server = servers[static_cast<size_t>(row)];

    return this->createIndex(row, column, const_cast<ServerData*>(&server));
}

QModelIndex FileServersModel::
parent(QModelIndex const&) const
{
    return {};
}

int FileServersModel::
rowCount(QModelIndex const& parent/*= {}*/) const
{
    if (parent.isValid()) return 0;

    auto const& servers = this->servers();

    return boost::numeric_cast<int>(servers.size());
}

int FileServersModel::
columnCount(QModelIndex const& parent/*= {}*/) const
{
    assert(!parent.isValid()); (void)parent;

    return Column::Count;
}

QVariant FileServersModel::
data(QModelIndex const& index, int const role/*= Qt::DisplayRole*/) const
{
    if (role != Qt::DisplayRole) return {};

    assert(index.isValid());

    auto* const server = static_cast<ServerData const*>(index.internalPointer());
    assert(server);

    auto const column = index.column();

    switch (column) {
    case Uri:
        return getUri(*server);
    case CurrentUri:
        return getCurrentUri(*server);
    case DownloadSpeed:
        return getDownloadSpeed(*server);
    default:
        throw std::logic_error("FileServersModel::data");
    }
}

QVariant FileServersModel::
headerData(int const section, Qt::Orientation const orientation,
                              int const role/*= Qt::DisplayRole*/) const
{
    if (role != Qt::DisplayRole) return {};

    assert(orientation == Qt::Horizontal); (void)orientation;

    switch (section) {
    case Uri:
        return "URI";
    case CurrentUri:
        return "Current URI";
    case DownloadSpeed:
        return "Download Speed";
    default:
        throw std::logic_error("FileServersModel::headerData");
    }
}

void FileServersModel::
onDataChanged()
{
    this->endResetModel();
}

ServerDatas const& FileServersModel::
servers() const
{
    static ServerDatas const empty;

    if (m_handle && m_fileData) {
        auto const idx = m_fileData->index();
        return m_handle->servers(idx);
    }
    else {
        return empty;
    }
}

} // namespace aria2_remote

