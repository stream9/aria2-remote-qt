#include "files_model.hpp"

#include "core/download_handle.hpp"
#include "core/file_data.hpp"

#include <cassert>

#include <boost/numeric/conversion/cast.hpp>

#include <QModelIndex>
#include <QVariant>

namespace aria2_remote {

static QVariant
getIndex(FileData const& file)
{
    return file.index();
}

static QVariant
getSelected(FileData const& file)
{
    return file.selected();
}

static QVariant
getPath(FileData const& file)
{
    return file.path();
}

static QVariant
getLength(FileData const& file)
{
    return QVariant::fromValue(file.length());
}

static QVariant
getCompletedLength(FileData const& file)
{
    return QVariant::fromValue(file.completedLength());
}

// FilesModel

void FilesModel::
setHandle(DownloadHandle const& handle)
{
    if (&handle == m_handle) return;

    disconnectFromHandle();

    m_handle = &handle;
    assert(m_handle);

    connectToHandle();

    onDownloadHandleChanged();
}

void FilesModel::
resetHandle()
{
    disconnectFromHandle();

    m_handle = nullptr;

    onDownloadHandleChanged();
}

QModelIndex FilesModel::
index(int const row, int const column,
                     QModelIndex const& parent/*= {}*/) const
{
    assert(row >= 0);
    assert(column >= 0);
    assert(column < Column::Count);
    assert(!parent.isValid()); (void)parent;

    if (m_handle) {
        auto const& files = m_handle->files();
        assert(static_cast<size_t>(row) < files.size());

        auto const& fileData = files[static_cast<size_t>(row)];

        return this->createIndex(row, column, const_cast<FileData*>(&fileData));
    }
    else {
        return this->createIndex(row, column);
    }
}

QModelIndex FilesModel::
parent(QModelIndex const&) const
{
    return {};
}

int FilesModel::
rowCount(QModelIndex const& parent/*= {}*/) const
{
    if (parent.isValid()) return 0;

    if (!m_handle) return 0;

    auto const& files = m_handle->files();

    return boost::numeric_cast<int>(files.size());
}

int FilesModel::
columnCount(QModelIndex const& parent/*= {}*/) const
{
    assert(!parent.isValid()); (void)parent;

    return Column::Count;
}

QVariant FilesModel::
data(QModelIndex const& index, int const role/*= Qt::DisplayRole*/) const
{
    if (role != Qt::DisplayRole) return {};

    assert(index.isValid());

    auto* const fileData = static_cast<FileData const*>(index.internalPointer());
    if (!fileData) return {};

    auto const column = index.column();

    switch (column) {
    case Index:
        return getIndex(*fileData);
    case Selected:
        return getSelected(*fileData);
    case Path:
        return getPath(*fileData);
    case Length:
        return getLength(*fileData);
    case CompletedLength:
        return getCompletedLength(*fileData);
    default:
        throw std::logic_error("FilesModel::data");
    }
}

QVariant FilesModel::
headerData(int const section, Qt::Orientation const orientation,
                              int const role/*= Qt::DisplayRole*/) const
{
    if (role != Qt::DisplayRole) return {};

    assert(orientation == Qt::Horizontal); (void)orientation;

    switch (section) {
    case Index:
        return "Index";
    case Selected:
        return "Selected";
    case Path:
        return "Path";
    case Length:
        return "Length";
    case CompletedLength:
        return "Completed Length";
    default:
        throw std::logic_error("FilesModel::headerData");
    }
}

void FilesModel::
connectToHandle()
{
    assert(m_handle);

    this->connect(m_handle,  &DownloadHandle::changed,
                  this,      &FilesModel::onDownloadHandleChanged);

    this->connect(m_handle, &QObject::destroyed,
                  this,     &FilesModel::resetHandle);
}

void FilesModel::
disconnectFromHandle()
{
    if (!m_handle) return;

    this->disconnect(m_handle,  &DownloadHandle::changed,
                     this,      &FilesModel::onDownloadHandleChanged);

    this->disconnect(m_handle, &QObject::destroyed,
                     this,     &FilesModel::resetHandle);
}

void FilesModel::
onDownloadHandleChanged()
{
    this->endResetModel();
}

} // namespace aria2_remote
