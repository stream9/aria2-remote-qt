#include "uri_general_panel.hpp"

#include "core/application.hpp"
#include "core/aria2_remote.hpp"
#include "core/download_handle.hpp"
#include "piece_widget.hpp"

#include <cassert>

#include <QFormLayout>
#include <QLabel>

#include <QDebug>

namespace aria2_remote {

static void
appendRow(QFormLayout& layout, QLabel& label, QString const& name)
{
    label.setTextInteractionFlags(Qt::TextSelectableByMouse);
    layout.addRow(name, &label);
}

UriGeneralPanel::
UriGeneralPanel(QWidget& parent)
    : QWidget { &parent }
    , m_gid { make_qmanaged<QLabel>(this) }
    , m_status { make_qmanaged<QLabel>(this) }
    , m_dir { make_qmanaged<QLabel>(this) }
    , m_totalLength { make_qmanaged<QLabel>(this) }
    , m_completedLength { make_qmanaged<QLabel>(this) }
    , m_connections { make_qmanaged<QLabel>(this) }
    , m_downloadSpeed { make_qmanaged<QLabel>(this) }
    , m_pieceLength { make_qmanaged<QLabel>(this) }
    , m_numPieces { make_qmanaged<QLabel>(this) }
    , m_bitField { make_qmanaged<PieceWidget>(*this) }
    , m_errorMessage { make_qmanaged<QLabel>(this) }
{
    auto layout = make_qmanaged<QFormLayout>(this);

    appendRow(*layout, *m_gid, "GID:");
    appendRow(*layout, *m_status, "Status:");
    appendRow(*layout, *m_dir, "Download Directory:");
    appendRow(*layout, *m_totalLength, "Total Length:");
    appendRow(*layout, *m_completedLength, "Completed Length:");
    appendRow(*layout, *m_connections, "Connections:");
    appendRow(*layout, *m_downloadSpeed, "Download Speed:");
    appendRow(*layout, *m_pieceLength, "Piece Length:");
    appendRow(*layout, *m_numPieces, "# of Pieces:");

    layout->addRow("Pieces:", m_bitField.get());

    appendRow(*layout, *m_errorMessage, "Error:");

    this->connect(&gApp(), &Application::currentHandleChanged,
                  this,    &UriGeneralPanel::setDownloadHandle);

    setDownloadHandle(gApp().currentHandle());
}

void UriGeneralPanel::
setDownloadHandle(DownloadHandle const* handle)
{
    if (handle == m_handle) return;

    if (m_handle) {
        m_handle->disconnect(this);
    }

    m_handle = handle;
    m_bitField->setHandle(handle);

    if (m_handle) {
        this->connect(m_handle, &DownloadHandle::changed,
                      this,     &UriGeneralPanel::refresh);
    }

    refresh();
}

void UriGeneralPanel::
refresh()
{
    if (m_handle) {
        //TODO format properly
        m_gid->setText(m_handle->gid());
        m_status->setText(m_handle->status().text());
        m_dir->setText(m_handle->dir());
        m_totalLength->setText(QString::number(m_handle->totalLength()));
        m_completedLength->setText(QString::number(m_handle->completedLength()));
        m_connections->setText(QString::number(m_handle->connections()));
        m_downloadSpeed->setText(QString::number(m_handle->downloadSpeed()));
        m_pieceLength->setText(QString::number(m_handle->pieceLength()));
        m_numPieces->setText(QString::number(m_handle->numPieces()));
        m_errorMessage->setText(m_handle->errorMessage());
    }
    else {
        m_gid->clear();
        m_status->clear();
        m_dir->clear();
        m_totalLength->clear();
        m_completedLength->clear();
        m_connections->clear();
        m_downloadSpeed->clear();
        m_pieceLength->clear();
        m_numPieces->clear();
        m_errorMessage->clear();
    }
}

void UriGeneralPanel::
onHandleDestroyed()
{
    setDownloadHandle(nullptr);
}

} // namespace aria2_remote
