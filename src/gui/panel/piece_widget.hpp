#ifndef ARIA2_REMOTE_PIECE_WIDGET_HPP
#define ARIA2_REMOTE_PIECE_WIDGET_HPP

#include <QFrame>

class QColor;
class QPaintEvent;
class QPainter;
class QRect;
class QSize;

namespace aria2_remote {

class DownloadHandle;

class PieceWidget : public QFrame
{
    Q_OBJECT
public:
    PieceWidget(QWidget& parent);

    void setHandle(DownloadHandle const*);

    // override QWidget
    QSize sizeHint() const override;

private:
    Q_SLOT void refresh();

    // override QWidget
    void paintEvent(QPaintEvent*) override;

private:
    DownloadHandle const* m_handle = nullptr;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_PIECE_WIDGET_HPP
