#ifndef ARIA2_REMOTE_FILES_MODEL_HPP
#define ARIA2_REMOTE_FILES_MODEL_HPP

#include <QAbstractItemModel>

class QModelIndex;
class QVariant;

namespace aria2_remote {

class DownloadHandle;

class FilesModel : public QAbstractItemModel
{
    Q_OBJECT

    enum Column { Index = 0, Selected, Path, Length, CompletedLength, Count };
public:
    FilesModel() = default;

    void setHandle(DownloadHandle const&);
    Q_SLOT void resetHandle();

    // override QAbstractItemModel
    QModelIndex index(int row, int column,
                      QModelIndex const& parent = {}) const override;
    QModelIndex parent(QModelIndex const&) const override;
    int rowCount(QModelIndex const& parent = {}) const override;
    int columnCount(QModelIndex const& parent = {}) const override;
    QVariant data(QModelIndex const&, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation, int role = Qt::DisplayRole) const override;

private:
    void connectToHandle();
    void disconnectFromHandle();

    Q_SLOT void onDownloadHandleChanged();

private:
    DownloadHandle const* m_handle = nullptr;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_FILES_MODEL_HPP

