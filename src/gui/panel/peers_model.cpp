#include "peers_model.hpp"

#include "core/download_handle.hpp"
#include "core/download_peers.hpp"

#include <cassert>

#include <boost/numeric/conversion/cast.hpp>

#include <QModelIndex>
#include <QVariant>

#include <QDebug>

namespace aria2_remote {

static QVariant
getId(Peer const& peer)
{
    return peer.id();
}

static QVariant
getAddress(Peer const& peer)
{
    return peer.address();
}

static QVariant
getPort(Peer const& peer)
{
    return peer.port();
}

static QVariant
getProgress(Peer const& peer)
{
    static QString const fmt { "%1 / %2" };
    auto const& bitField = peer.bitField();

    return fmt.arg(bitField.count()).arg(bitField.size());
}

static QVariant
getDownloadSpeed(Peer const& peer)
{
    return peer.downloadSpeed();
}

static QVariant
getUploadSpeed(Peer const& peer)
{
    return peer.uploadSpeed();
}

static QVariant
getChoking(Peer const& peer)
{
    return peer.isChoking();
}

static QVariant
getChoked(Peer const& peer)
{
    return peer.isChoked();
}

static QVariant
getSeeder(Peer const& peer)
{
    return peer.isSeeder();
}

// PeersModel

PeersModel::~PeersModel() = default;

void PeersModel::
setHandle(DownloadHandle& handle)
{
    if (&handle == m_handle) return;

    m_handle = &handle;
    assert(m_handle);

    onDownloadHandleChanged();
}

void PeersModel::
resetHandle()
{
    m_handle = nullptr;

    onDownloadHandleChanged();
}

QModelIndex PeersModel::
index(int const row, int const column,
                     QModelIndex const& parent/*= {}*/) const
{
    assert(row >= 0);
    assert(column >= 0);
    assert(column < Column::Count);
    assert(!parent.isValid()); (void)parent;

    return this->createIndex(row, column);
}

QModelIndex PeersModel::
parent(QModelIndex const&) const
{
    return {};
}

int PeersModel::
rowCount(QModelIndex const& parent/*= {}*/) const
{
    if (parent.isValid()) return 0;

    if (!m_peers) return 0;

    return boost::numeric_cast<int>(m_peers->size());
}

int PeersModel::
columnCount(QModelIndex const& parent/*= {}*/) const
{
    assert(!parent.isValid()); (void)parent;

    return Column::Count;
}

QVariant PeersModel::
data(QModelIndex const& index, int const role/*= Qt::DisplayRole*/) const
{
    if (role != Qt::DisplayRole) return {};

    assert(index.isValid());

    // QSortFilterProxyModel cause this
    if (index.row() >= rowCount()) {
        return {};
    }

    if (!m_peers) return {};

    auto const& peer = (*m_peers)[static_cast<size_t>(index.row())];

    auto const column = index.column();

    switch (column) {
    case Id:
        return getId(peer);
    case Address:
        return getAddress(peer);
    case Port:
        return getPort(peer);
    case Progress:
        return getProgress(peer);
    case DownloadSpeed:
        return getDownloadSpeed(peer);
    case UploadSpeed:
        return getUploadSpeed(peer);
    case Choking:
        return getChoking(peer);
    case Choked:
        return getChoked(peer);
    case Seeder:
        return getSeeder(peer);
    default:
        throw std::logic_error("PeersModel::data");
    }
}

QVariant PeersModel::
headerData(int const section, Qt::Orientation const orientation,
                              int const role/*= Qt::DisplayRole*/) const
{
    if (role != Qt::DisplayRole) return {};

    assert(orientation == Qt::Horizontal); (void)orientation;

    switch (section) {
    case Id:
        return "ID";
    case Address:
        return "Address";
    case Port:
        return "Port";
    case Progress:
        return "Progress";
    case DownloadSpeed:
        return "Download Speed";
    case UploadSpeed:
        return "Upload Speed";
    case Choking:
        return "Choking";
    case Choked:
        return "Choked";
    case Seeder:
        return "Seeder";
    default:
        throw std::logic_error("PeersModel::headerData");
    }
}

void PeersModel::
onDownloadHandleChanged()
{
    if (m_handle) {
        if (m_peers) {
            m_peers->disconnect(this);
        }

        this->beginResetModel();

        m_peers = std::make_shared<DownloadPeers>(*m_handle);

        this->endResetModel();

        this->connect(m_peers.get(), &DownloadPeers::peerChanged,
                      this,          &PeersModel::onPeerChanged);
        this->connect(m_peers.get(), &DownloadPeers::peerCreated,
                      this,          &PeersModel::onPeerCreated);
        this->connect(m_peers.get(), &DownloadPeers::peerDeleted,
                      this,          &PeersModel::onPeerDeleted);
    }
    else {
        m_peers.reset();
    }
}

void PeersModel::
onPeerChanged(size_t const row)
{
    auto const index = this->index(boost::numeric_cast<int>(row), 0);
    Q_EMIT this->dataChanged(index, index);
}

void PeersModel::
onPeerCreated(size_t const row)
{
    auto const row_ = boost::numeric_cast<int>(row);

    this->beginInsertRows({}, row_, row_);
    this->endInsertRows();
}

void PeersModel::
onPeerDeleted(size_t const row)
{
    auto const row_ = boost::numeric_cast<int>(row);

    this->beginRemoveRows({}, row_, row_);
    this->endRemoveRows();
}

} // namespace aria2_remote
