#include "piece_widget.hpp"

#include "core/download_handle.hpp"

#include <cstdlib>
#include <ctime>

#include <QRect>
#include <QSize>
#include <QColor>
#include <QPaintEvent>
#include <QPainter>
#include <QSizePolicy>

#include <QDebug>

namespace aria2_remote {

PieceWidget::
PieceWidget(QWidget& parent)
    : QFrame { &parent }
{
    this->setFrameStyle(QFrame::Panel | QFrame::Sunken);
    this->setLineWidth(2);
    this->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);

    std::srand(static_cast<unsigned int>(std::time(0)));
}

void PieceWidget::
setHandle(DownloadHandle const* const handle)
{
    if (handle == m_handle) return;

    if (m_handle) {
        m_handle->disconnect(this);
    }

    m_handle = handle;

    if (m_handle) {
        this->connect(m_handle, &DownloadHandle::changed,
                      this,     &PieceWidget::refresh);
    }

    refresh();
}

QSize PieceWidget::
sizeHint() const
{
    return { 500, 20 };
}

static void
drawSegment(QPainter& painter,
            int const left, int const width,
            QRect const& rect,
            QColor const& color)
{
    painter.fillRect(
            rect.left() + left, rect.top(),
            width, rect.height(),
            color
    );
}

void PieceWidget::
refresh()
{
    this->update();
}

void PieceWidget::
paintEvent(QPaintEvent* const event)
{
    QFrame::paintEvent(event);

    if (!m_handle) return;

    QPainter painter { this };

    auto const& palette = this->palette();
    auto const& color = palette.color(QPalette::Highlight);
    auto const& bitField = m_handle->bitField();

    auto const rect = this->contentsRect();
    auto const ratio = rect.width() / static_cast<double>(bitField.size());
    size_t from = 0, len = 0;

    for (auto i = 0u; i < bitField.size(); ++i) {
        if (bitField.test(i)) {
            if (len == 0) {
                from = i;
                len = 1;
            }
            else {
                ++len;
            }
        }
        else {
            if (len > 0) {
                auto const left =
                    static_cast<int>(static_cast<double>(from) * ratio);
                auto const width =
                    static_cast<int>(static_cast<double>(len) * ratio);

                drawSegment(painter, left, width, rect, color);

                from = 0;
                len = 0;
            }
        }
    }

    if (len > 0) {
        auto const left =
            static_cast<int>(static_cast<double>(from) * ratio);
        auto const width =
            static_cast<int>(static_cast<double>(len) * ratio);

        drawSegment(painter, left, width, rect, color);
    }
}

} // namespace aria2_remote
