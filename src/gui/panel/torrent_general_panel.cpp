#include "torrent_general_panel.hpp"

#include "core/application.hpp"
#include "core/bt_meta_info_data.hpp"
#include "core/download_handle.hpp"
#include "piece_widget.hpp"

#include <QDateTime>
#include <QCheckBox>
#include <QFormLayout>
#include <QLabel>
#include <QTreeWidget>
#include <QTreeWidgetItem>

namespace aria2_remote {

static auto
createLabel(QWidget& parent)
{
    auto label = make_qmanaged<QLabel>(&parent);
    label->setTextInteractionFlags(Qt::TextSelectableByMouse);

    return label;
}

static auto
createSeedingCheck(QWidget& parent)
{
    auto check = make_qmanaged<QCheckBox>(&parent);
    check->setAttribute(Qt::WA_TransparentForMouseEvents, true);
    check->setFocusPolicy(Qt::NoFocus);

    return check;
}

static auto
createTree(QWidget& parent)
{
    auto tree = make_qmanaged<QTreeWidget>(&parent);
    tree->setHeaderHidden(true);

    return tree;
}

static void
updateAnnounceList(QTreeWidget& tree,
                   BtMetaInfoData::AnnounceList const& list)
{
    tree.clear();

    for (size_t index = 0; index < list.size(); ++index) {
        auto const& tier = list[index];
        auto const& label = QString("Tier %1").arg(index);

        // tree takes ownership
        auto& tierItem = to_ref(new QTreeWidgetItem(&tree));
        tierItem.setText(0, label);

        for (auto const& tracker: tier) {
            // tierItem takes ownership
            auto& trackerItem = to_ref(new QTreeWidgetItem(&tierItem));
            trackerItem.setText(0, tracker);
        }
    }

    tree.expandAll();
}

TorrentGeneralPanel::
TorrentGeneralPanel(QWidget& parent)
    : QScrollArea { &parent }
    , m_gid { createLabel(*this) }
    , m_name { createLabel(*this) }
    , m_infoHash { createLabel(*this) }
    , m_mode { createLabel(*this) }
    , m_dir { createLabel(*this) }
    , m_creationDate { createLabel(*this) }
    , m_comment { createLabel(*this) }
    , m_status { createLabel(*this) }
    , m_length { createLabel(*this) }
    , m_nodes { createLabel(*this) }
    , m_seeding { createSeedingCheck(*this) }
    , m_speed { createLabel(*this) }
    , m_piece { createLabel(*this) }
    , m_bitField { make_qmanaged<PieceWidget>(*this) }
    , m_announceList { createTree(*this) }
    , m_errorMessage { createLabel(*this) }
{
    auto panel = make_qmanaged<QWidget>(this);
    auto layout = make_qmanaged<QFormLayout>(panel.get());

    layout->addRow("GID:", m_gid.get());
    layout->addRow("Name:", m_name.get());
    layout->addRow("Info Hash:", m_infoHash.get());
    layout->addRow("File Mode:", m_mode.get());
    layout->addRow("Download Directory:", m_dir.get());
    layout->addRow("Creation Date:", m_creationDate.get());
    layout->addRow("Comment:", m_comment.get());

    layout->addRow("Status:", m_status.get());
    layout->addRow("Completed / Total Length:", m_length.get());
    layout->addRow("Seeders / Leachers:", m_nodes.get());
    layout->addRow("Seeding:", m_seeding.get());
    layout->addRow("Download / Upload Speed:", m_speed.get());
    layout->addRow("Piece Length / Number:", m_piece.get());
    layout->addRow("Pieces:", m_bitField.get());

    layout->addRow("Announce List:", m_announceList.get());

    layout->addRow("Error:", m_errorMessage.get());

    this->connect(&gApp(), &Application::currentHandleChanged,
                  this,    &TorrentGeneralPanel::setDownloadHandle);

    this->setFrameShape(QFrame::NoFrame);
    this->setWidget(panel.get());
    this->setEnabled(false);

    setDownloadHandle(gApp().currentHandle());
}

void TorrentGeneralPanel::
setDownloadHandle(DownloadHandle const* handle)
{
    if (handle == m_handle) return;

    if (m_handle) {
        m_handle->disconnect(this);
    }

    m_handle = handle;
    m_bitField->setHandle(handle);

    this->setEnabled(m_handle && m_handle->infoHash());

    if (m_handle) {
        this->connect(m_handle, &DownloadHandle::changed,
                      this,     &TorrentGeneralPanel::refresh);
    }

    refresh();
}

void TorrentGeneralPanel::
refresh()
{
    if (!this->isEnabled()) {
        m_gid->clear();
        m_name->clear();
        m_infoHash->clear();
        m_mode->clear();
        m_dir->clear();
        m_creationDate->clear();
        m_comment->clear();
        m_status->clear();
        m_length->clear();
        m_nodes->clear();
        m_seeding->setCheckState(Qt::Unchecked);
        m_speed->clear();
        m_piece->clear();
        m_announceList->clear();
        m_errorMessage->clear();
    }
    else {
        assert(m_handle);

        m_gid->setText(m_handle->gid());

        auto* const metaInfo = m_handle->bitTorrent();
        assert(metaInfo);

        if (auto const& name = metaInfo->name()) {
            m_name->setText(*name);
        }
        else {
            m_name->clear();
        }

        auto* const infoHash = m_handle->infoHash();
        assert(infoHash);
        m_infoHash->setText(*infoHash);

        using BtFileType = BtMetaInfoData::BtFileType;
        if (auto const& mode = metaInfo->mode(); !mode) {
            m_mode->clear();
        }
        else if (*mode == BtFileType::Single) {
            m_mode->setText("Single");
        }
        else if (*mode == BtFileType::Multi) {
            m_mode->setText("Multi");
        }
        else {
            assert(false && "unknown bittorrent file type");
        }

        m_dir->setText(m_handle->dir());

        if (auto const& creationDate = metaInfo->creationDate()) {
            auto const& date = QDateTime::fromSecsSinceEpoch(*creationDate);
            m_creationDate->setText(date.toString());
        }
        else {
            m_creationDate->clear();
        }

        if (auto const& comment = metaInfo->comment()) {
            m_comment->setText(*comment);
        }
        else {
            m_comment->clear();
        }

        m_status->setText(m_handle->status().text());

        auto const& length =
            QString("%1 / %2").arg(m_handle->completedLength())
                              .arg(m_handle->totalLength());
        m_length->setText(length);

        auto const& seeders = m_handle->numSeeders();
        assert(seeders);
        auto const leeches = m_handle->connections() - *seeders;
        m_nodes->setText(QString("%1 / %2").arg(*seeders).arg(leeches));

        auto const& seeding = m_handle->seeder();
        m_seeding->setCheckState(seeding && *seeding ? Qt::Checked : Qt::Unchecked);

        m_speed->setText(
            QString("%1 / %2").arg(m_handle->downloadSpeed())
                              .arg(m_handle->uploadSpeed()));

        m_piece->setText(
            QString("%1 / %2").arg(m_handle->pieceLength())
                              .arg(m_handle->numPieces()));

        updateAnnounceList(*m_announceList, metaInfo->announceList());

        m_errorMessage->setText(m_handle->errorMessage());
    }
}

} // namespace aria2_remote
