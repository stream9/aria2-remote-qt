#ifndef ARIA2_REMOTE_FILES_PANEL_HPP
#define ARIA2_REMOTE_FILES_PANEL_HPP

#include "utility/pointer.hpp"

#include <QWidget>

class QModelIndex;
class QSplitter;
class QTabWidget;

namespace aria2_remote {

class DownloadHandle;

class FilesPanel : public QWidget
{
    Q_OBJECT

    class FilesView;
    class ServersView;
    class UrisView;

public:
    FilesPanel();
    ~FilesPanel();

    void readSettings();
    void writeSettings();

    Q_SLOT void setDownloadHandle(DownloadHandle*);
    Q_SLOT void setCurrentFile(QModelIndex const&);

private:
    DownloadHandle* m_handle = nullptr;

    qmanaged_ptr<QSplitter> m_splitter;
    qmanaged_ptr<QTabWidget> m_tab;

    qmanaged_ptr<FilesView> m_files;
    qmanaged_ptr<UrisView> m_uris;
    qmanaged_ptr<ServersView> m_servers;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_FILES_PANEL_HPP
