#ifndef ARIA2_REMOTE_PEERS_MODEL_HPP
#define ARIA2_REMOTE_PEERS_MODEL_HPP

#include "core/download_handle.hpp"

#include <memory>

#include <QAbstractItemModel>
#include <QPointer>

class QModelIndex;
class QVariant;

namespace aria2_remote {

class DownloadPeers;

class PeersModel : public QAbstractItemModel
{
    Q_OBJECT

    enum Column { Id = 0, Address, Port, Progress, DownloadSpeed,
                  UploadSpeed, Choking, Choked, Seeder, Count };
public:
    PeersModel() = default;
    ~PeersModel();

    void setHandle(DownloadHandle&);
    Q_SLOT void resetHandle();

    // override QAbstractItemModel
    QModelIndex index(int row, int column,
                      QModelIndex const& parent = {}) const override;
    QModelIndex parent(QModelIndex const&) const override;
    int rowCount(QModelIndex const& parent = {}) const override;
    int columnCount(QModelIndex const& parent = {}) const override;
    QVariant data(QModelIndex const&, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation, int role = Qt::DisplayRole) const override;

private:
    void connectToHandle();
    void disconnectFromHandle();

    Q_SLOT void onDownloadHandleChanged();

    Q_SLOT void onPeerChanged(size_t row);
    Q_SLOT void onPeerCreated(size_t row);
    Q_SLOT void onPeerDeleted(size_t row);

private:
    QPointer<DownloadHandle> m_handle = nullptr;
    std::shared_ptr<DownloadPeers> m_peers;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_PEERS_MODEL_HPP

