#ifndef ARIA2_REMOTE_URIS_PANEL_HPP
#define ARIA2_REMOTE_URIS_PANEL_HPP

#include "utility/pointer.hpp"

#include <memory>

#include <QWidget>

class QAbstractItemModel;

namespace aria2_remote {

class DownloadHandle;
class TreeView;

class UrisPanel : public QWidget
{
    Q_OBJECT
public:
    UrisPanel();
    ~UrisPanel() override;

    void readSettings();
    void writeSettings() const;

private:
    Q_SLOT void setDownloadHandle(DownloadHandle*);

private:
    DownloadHandle const* m_handle = nullptr;

    qmanaged_ptr<TreeView> m_uris;

    std::unique_ptr<QAbstractItemModel> m_model; // non-null
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_URIS_PANEL_HPP
