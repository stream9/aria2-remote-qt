#include "peers_panel.hpp"

#include "peers_model.hpp"

#include "core/application.hpp"
#include "core/download_handle.hpp"
#include "core/download_peers.hpp"
#include "gui/tree_view.hpp"

#include <cassert>

#include <QBoxLayout>

#include <QDebug>

namespace aria2_remote {

PeersPanel::
PeersPanel()
    : m_uris { make_qmanaged<TreeView>(this) }
    , m_model { std::make_unique<PeersModel>() }
{
    m_sortModel.setSourceModel(m_model.get());

    auto layout = make_qmanaged<QVBoxLayout>(this);

    m_uris->setObjectName("PeersPanel TreeView");
    m_uris->setModel(&m_sortModel);
    m_uris->setSortingEnabled(true);

    layout->addWidget(m_uris.get());

    this->connect(&gApp(), &Application::currentHandleChanged,
                  this,    &PeersPanel::setDownloadHandle);
    this->connect(&gApp(), &Application::readSettings,
                  this,    &PeersPanel::readSettings);
    this->connect(&gApp(), &Application::writeSettings,
                  this,    &PeersPanel::writeSettings);

    this->setEnabled(false);
}

PeersPanel::~PeersPanel() = default;

void PeersPanel::
readSettings()
{
    m_uris->readSettings();
}

void PeersPanel::
writeSettings() const
{
    m_uris->writeSettings();
}

void PeersPanel::
setDownloadHandle(DownloadHandle* const handle)
{
    if (handle == m_handle) return;

    if (m_handle) {
        m_handle->disconnect(this);
    }

    m_handle = handle;

    if (m_handle) {
        this->connect(m_handle, &QObject::destroyed,
                      this,     &PeersPanel::onHandleDestroyed);
    }

    if (handle && handle->isTorrent()) {
        m_model->setHandle(*handle);
        this->setEnabled(true);
    }
    else {
        m_model->resetHandle();
        this->setEnabled(false);
    }
}

void PeersPanel::
onHandleDestroyed()
{
    setDownloadHandle(nullptr);
}

} // namespace aria2_remote
