#ifndef ARIA2_REMOTE_URIS_MODEL_HPP
#define ARIA2_REMOTE_URIS_MODEL_HPP

#include "core/uri_data.hpp"
#include "core/download_handle.hpp"

#include <vector>

#include <QAbstractItemModel>

class QModelIndex;
class QVariant;

namespace aria2_remote {

class FileData;

class UrisModel : public QAbstractItemModel
{
    Q_OBJECT
public:
    enum Column { Status = 0, URI, Count };

protected:
    using UriDatas = std::vector<UriData>;

public:
    UrisModel(DownloadHandle*);

    void setHandle(DownloadHandle&);
    Q_SLOT void resetHandle();

    // override QAbstractItemModel
    QModelIndex index(int row, int column,
                      QModelIndex const& parent = {}) const override;
    QModelIndex parent(QModelIndex const&) const override;
    int rowCount(QModelIndex const& parent = {}) const override;
    int columnCount(QModelIndex const& parent = {}) const override;
    QVariant data(QModelIndex const&, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation, int role = Qt::DisplayRole) const override;

protected:
    DownloadHandle* handle() { return m_handle; }
    DownloadHandle const* handle() const { return m_handle; }

private:
    virtual UriDatas const& uris() const = 0;

private:
    QVariant toolTipData(QModelIndex const& index) const;

    void connectToHandle();
    void disconnectFromHandle();

    Q_SLOT void onDownloadHandleChanged();

private:
    DownloadHandle* m_handle = nullptr;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_URIS_MODEL_HPP
