#include "general_panel.hpp"

#include "core/application.hpp"
#include "core/download_handle.hpp"
#include "torrent_general_panel.hpp"
#include "uri_general_panel.hpp"

#include <QStackedLayout>

namespace aria2_remote {

GeneralPanel::
GeneralPanel()
    : m_stack { make_qmanaged<QStackedLayout>(this) }
    , m_uriPanel { make_qmanaged<UriGeneralPanel>(*this) }
    , m_torrentPanel { make_qmanaged<TorrentGeneralPanel>(*this) }
{
    m_stack->addWidget(m_uriPanel.get());
    m_stack->addWidget(m_torrentPanel.get());

    this->connect(&gApp(), &Application::currentHandleChanged,
                  this,            &GeneralPanel::selectPanel);

    selectPanel(gApp().currentHandle());
}

void GeneralPanel::
selectPanel(DownloadHandle* const handle)
{
    if (handle) {
        if (handle->infoHash()) {
            m_stack->setCurrentWidget(m_torrentPanel.get());
        }
        else {
            m_stack->setCurrentWidget(m_uriPanel.get());
        }

        this->setEnabled(true);
    }
    else {
        m_stack->setCurrentWidget(m_uriPanel.get());

        this->setEnabled(false);
    }
}

} // namespace aria2_remote
