#ifndef ARIA2_REMOTE_ACTION_QUIT_HPP
#define ARIA2_REMOTE_ACTION_QUIT_HPP

#include "base.hpp"

namespace aria2_remote {

class ActionManager;

namespace action {

class Quit : public Base
{
public:
    Quit(ActionManager&);

private:
    void run() override;
    void update() override;
};

}} // namespace aria2_remote::action

#endif // ARIA2_REMOTE_ACTION_QUIT_HPP
