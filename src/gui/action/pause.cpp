#include "pause.hpp"

#include "core/application.hpp"
#include "core/download_handle.hpp"
#include "gui/icons.hpp"

#include <algorithm>
#include <cassert>

namespace aria2_remote::action {

static auto isPausable = [](auto const& handle)
{
    if (!handle) return false;

    auto const& status = handle->status();

    return status == DownloadStatus::Active
        || status == DownloadStatus::Waiting;
};

static auto isAllPausable = [](auto const& selection)
{
    return std::all_of(selection.begin(), selection.end(),
        [](auto const& weak) {
            auto const& handle = weak.lock();
            return isPausable(handle);
        });
};

static auto isUnpausable = [](auto const& handle)
{
    if (!handle) return false;

    auto const& status = handle->status();

    return status == DownloadStatus::Paused;
};

static auto isAllUnpausable = [](auto const& selection)
{
    return std::all_of(selection.begin(), selection.end(),
        [](auto const& weak) {
            auto const& handle = weak.lock();
            return isUnpausable(handle);
        });
};

// Pause

Pause::
Pause(ActionManager& manager, Application& app)
    : Base { manager }
    , m_application { app }
{
    this->setIcon(QIcon::fromTheme("media-playback-pause"));
    update();

    m_updateTimer.setInterval(300);
    m_updateTimer.setSingleShot(true);
    this->connect(&m_updateTimer, &QTimer::timeout,
                  this,           &Pause::update);

    this->connect(&m_application, &Application::selectionChanged,
                  this,           &Pause::updateSelection);
}

void Pause::
run()
{
    for (auto const& weak: m_selection) {
        auto const& handle = weak.lock();
        if (!handle) continue;

        if (isPausable(handle)) {
            handle->pause();
        }
        else if (isUnpausable(handle)) {
            handle->unpause();
        }
        else {
            //TODO properly handle this situation. ex. display error dialog.
            assert(false);
        }
    }
}

void Pause::
update()
{
    updateSelection();

    if (m_selection.empty()) {
        this->setText("&Pause");
        this->setEnabled(false);
    }
    else if (isAllPausable(m_selection)) {
        this->setText("&Pause");
        this->setEnabled(true);
    }
    else if (isAllUnpausable(m_selection)) {
        this->setText("&Unpause");
        this->setEnabled(true);
    }
    else {
        this->setText("&Pause");
        this->setEnabled(false);
    }
}

void Pause::
updateSelection()
{
    for (auto const& weak: m_selection) {
        auto const & handle = weak.lock();
        if (!handle) continue;

        handle->disconnect(&m_updateTimer);
    }

    m_selection.clear();

    for (DownloadHandle& handle: m_application.selection()) {
        m_selection.push_back(handle.weak_from_this());

        this->connect(&handle,        &DownloadHandle::statusChanged,
                      &m_updateTimer, qOverload<>(&QTimer::start));
    }
}

} // namespace aria2_remote::action
