#include "base.hpp"

#include "gui/action_manager.hpp"

#include <QDebug>

namespace aria2_remote::action {

Base::
Base(ActionManager& manager)
    : m_manager { manager }
{
    this->connect(this, &QAction::triggered,
                  this, &Base::run);

    this->connect(&manager, &ActionManager::updateRequest,
                  this,     &Base::update);
}

} // namespace aria2_remote::action
