#include "option.hpp"

#include "core/application.hpp"
#include "core/aria2_remote.hpp"
#include "gui/dialog/option_dialog.hpp"
#include "gui/icons.hpp"
#include "gui/main_window.hpp"
#include "utility/pointer.hpp"

#include <cassert>

#include <QApplication>

namespace aria2_remote::action {

Option::
Option(ActionManager& manager, Application& app)
    : Base { manager }
    , m_application { app }
{
    this->setText("&Option");
    this->setIcon(icons::option());

    this->connect(&m_application, &Application::aria2Changed,
                  this,           &Option::update);
}

void Option::
run()
{
    auto* const aria2 = m_application.aria2();
    if (!aria2) return;

    OptionDialog dialog { *aria2, m_application.mainWindow() };

    dialog.exec();
}

void Option::
update()
{
    if (m_application.aria2()) {
        this->setEnabled(true);
    }
    else {
        this->setEnabled(false);
    }
}

} // namespace aria2_remote::action
