#ifndef ARIA2_REMOTE_ACTION_OPTION_HPP
#define ARIA2_REMOTE_ACTION_OPTION_HPP

#include "base.hpp"

namespace aria2_remote {

class ActionManager;
class Application;

namespace action {

class Option : public Base
{
public:
    Option(ActionManager&, Application&);

private:
    void run() override;
    void update() override;

private:
    Application& m_application;
};

}} // namespace aria2_remote::action

#endif // ARIA2_REMOTE_ACTION_OPTION_HPP

