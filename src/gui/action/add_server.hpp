#ifndef ARIA2_REMOTE_ACTION_ADD_SERVER_HPP
#define ARIA2_REMOTE_ACTION_ADD_SERVER_HPP

#include "base.hpp"

namespace aria2_remote {

class ActionManager;
class Servers;

namespace action {

class AddServer : public Base
{
public:
    AddServer(ActionManager&, Servers&);

private:
    void run() override;
    void update() override;

private:
    Servers& m_servers;
};

}} // namespace aria2_remote::action

#endif // ARIA2_REMOTE_ACTION_ADD_SERVER_HPP
