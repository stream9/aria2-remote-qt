#include "edit_server.hpp"

#include "core/application.hpp"
#include "gui/dialog/server_dialog.hpp"
#include "gui/icons.hpp"
#include "core/server.hpp"
#include "utility/pointer.hpp"

#include <cassert>

#include <QApplication>

namespace aria2_remote::action {

EditServer::
EditServer(ActionManager& manager, Application& app)
    : Base { manager }
    , m_application { app }
{
    this->setText("&Edit Server");
    this->setIcon(icons::editServer());

    this->connect(&m_application, &Application::currentServerChanged,
                  this,           &EditServer::update);
    update();
}

void EditServer::
run()
{
    auto* const server = m_application.currentServer();
    if (!server) return;

    auto& servers = m_application.servers();
    auto& window = to_ref(QApplication::activeWindow());

    ServerDialog dialog { window, servers };
    dialog.setWindowTitle("Edit Server");
    dialog.setServer(*server);

    auto const result = dialog.exec();
    if (result == QDialog::Accepted) {
        server->assign(
            dialog.name(),
            dialog.host(),
            dialog.port(),
            dialog.token()
        );
    }
}

void EditServer::
update()
{
    this->setEnabled(m_application.currentServer());
}

} // namespace aria2_remote::action
