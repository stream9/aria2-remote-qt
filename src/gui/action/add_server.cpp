#include "add_server.hpp"

#include "gui/dialog/server_dialog.hpp"
#include "gui/icons.hpp"
#include "core/server.hpp"
#include "utility/pointer.hpp"

#include <cassert>

#include <QApplication>

namespace aria2_remote::action {

AddServer::
AddServer(ActionManager& manager, Servers& server)
    : Base { manager }
    , m_servers { server }
{
    this->setText("&Add Server");
    this->setIcon(icons::addServer());
}

void AddServer::
run()
{
    auto& window = to_ref(QApplication::activeWindow());

    ServerDialog dialog { window, m_servers };
    dialog.setWindowTitle("Add Server");

    auto const result = dialog.exec();
    if (result == QDialog::Accepted) {
        m_servers.append(
              dialog.name()
            , dialog.host()
            , dialog.port()
            , dialog.token()
        );
    }
}

void AddServer::
update()
{
}

} // namespace aria2_remote::action
