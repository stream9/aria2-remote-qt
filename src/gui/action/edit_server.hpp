#ifndef ARIA2_REMOTE_ACTION_EDIT_SERVER_HPP
#define ARIA2_REMOTE_ACTION_EDIT_SERVER_HPP

#include "base.hpp"

namespace aria2_remote {

class ActionManager;
class Application;

namespace action {

class EditServer : public Base
{
public:
    EditServer(ActionManager&, Application&);

private:
    void run() override;
    Q_SLOT void update() override;

private:
    Application& m_application;
};

}} // namespace aria2_remote::action

#endif // ARIA2_REMOTE_ACTION_EDIT_SERVER_HPP
