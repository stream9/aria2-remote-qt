#include "add_uri.hpp"

#include "core/application.hpp"
#include "core/aria2_remote.hpp"
#include "core/profile.hpp"
#include "core/rpc/aria2_rpc.hpp"
#include "gui/dialog/add_uri_dialog.hpp"
#include "gui/icons.hpp"
#include "json_rpc/response.hpp"
#include "utility/pointer.hpp"

#include <cassert>

#include <QJsonArray>
#include <QJsonObject>
#include <QApplication>
#include <QInputDialog>

#include <QDebug>

namespace aria2_remote::action {

AddUri::
AddUri(ActionManager& manager, Application& app)
    : Base { manager }
    , m_application { app }
{
    this->setText("&Add URI");
    this->setIcon(icons::addUri());

    this->connect(&m_application, &Application::aria2Changed,
                  this,           &AddUri::update);
}

void AddUri::
run()
{
    auto* const aria2 = m_application.aria2();
    if (!aria2) return;

    auto& window = to_ref(QApplication::activeWindow());
    AddUriDialog dialog { window };

    auto const result = dialog.exec();

    if (result == QDialog::Accepted) {
        QJsonArray uris;
        for (auto const& uri: dialog.uris()) {
            uris.push_back(uri);
        }

        Profile const profile { dialog.profile() };

        aria2->rpc().addUri(uris, profile.toJson(),
            [&](auto const& res) {
                assert(res); (void)res; //TODO properly

                assert(res.value().isString());
            }
        );
    }
}

void AddUri::
update()
{
    if (m_application.aria2()) {
        this->setEnabled(true);
    }
    else {
        this->setEnabled(false);
    }
}

} // namespace aria2_remote::action
