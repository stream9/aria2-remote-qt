#ifndef ARIA2_REMOTE_HEADER_CONTEXT_MENU_HPP
#define ARIA2_REMOTE_HEADER_CONTEXT_MENU_HPP

#include <QMenu>

class QAbstractItemModel;
class QHeaderView;

namespace aria2_remote {

class HeaderContextMenu : public QMenu
{
    Q_OBJECT
public:
    HeaderContextMenu(QHeaderView& header,
                      QAbstractItemModel const&);
private:
    Q_SLOT void onToggled(bool const checked);

private:
    QHeaderView& m_header;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_HEADER_CONTEXT_MENU_HPP
