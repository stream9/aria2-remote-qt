#include "profile_dialog.hpp"

#include "core/application.hpp"
#include "core/settings.hpp"

#include <cassert>

#include <QBoxLayout>
#include <QDialogButtonBox>
#include <QListWidget>
#include <QPushButton>

#include <QDebug>

namespace aria2_remote {

static QListWidgetItem&
newItem(QString const& name)
{
    auto& item = to_ref(new QListWidgetItem { name });
    item.setFlags(item.flags() | Qt::ItemIsEditable);

    return item;
}

ProfileDialog::
ProfileDialog(QWidget& parent)
    : QDialog { &parent }
    , m_profiles { make_qmanaged<QListWidget>(this) }
    , m_add { make_qmanaged<QPushButton>(this) }
    , m_remove { make_qmanaged<QPushButton>(this) }
    , m_up { make_qmanaged<QPushButton>(this) }
    , m_down { make_qmanaged<QPushButton>(this) }
    , m_buttonBox { make_qmanaged<QDialogButtonBox>(this) }
{
    auto layout = make_qmanaged<QVBoxLayout>(this);

    createTopPanel(*layout);
    createBottomButtonBox(*layout);

    load();

    updateWidgets();

    assert(m_profiles);
    assert(m_add);
    assert(m_remove);
    assert(m_buttonBox);
}

ProfileDialog::~ProfileDialog() = default;

void ProfileDialog::
createTopPanel(QBoxLayout& parent)
{
    auto layout = make_qmanaged<QHBoxLayout>();

    layout->addWidget(m_profiles.get());

    this->connect(m_profiles.get(), &QListWidget::itemSelectionChanged,
                  this,             &ProfileDialog::updateWidgets);

    this->connect(m_profiles.get(), &QListWidget::itemChanged,
                  this,             &ProfileDialog::onEdited);

    createSideButtons(*layout);

    parent.addLayout(layout.get());
}

void ProfileDialog::
createSideButtons(QBoxLayout& parent)
{
    auto layout = make_qmanaged<QVBoxLayout>();

    m_add->setText("&Add");
    layout->addWidget(m_add.get());

    this->connect(m_add.get(), &QPushButton::clicked,
                  this,        &ProfileDialog::addProfile);

    m_remove->setText("&Remove");
    layout->addWidget(m_remove.get());

    this->connect(m_remove.get(), &QPushButton::clicked,
                  this,           &ProfileDialog::removeProfile);

    m_up->setText("&Up");
    layout->addWidget(m_up.get());

    this->connect(m_up.get(), &QPushButton::clicked,
                  this,       &ProfileDialog::moveUp);

    m_down->setText("&Down");
    layout->addWidget(m_down.get());

    this->connect(m_down.get(), &QPushButton::clicked,
                  this,         &ProfileDialog::moveDown);

    layout->addStretch();

    parent.addLayout(layout.get());
}

void ProfileDialog::
createBottomButtonBox(QBoxLayout& parent)
{
    m_buttonBox->addButton(QDialogButtonBox::Ok);
    m_buttonBox->addButton(QDialogButtonBox::Cancel);

    this->connect(m_buttonBox.get(), &QDialogButtonBox::accepted,
                  this,              &ProfileDialog::onAccepted);

    this->connect(m_buttonBox.get(), &QDialogButtonBox::rejected,
                  this,              &ProfileDialog::reject);

    parent.addWidget(m_buttonBox.get());
}

void ProfileDialog::
load()
{
    auto& settings = gApp().settings();

    auto const& profiles = settings.profiles();

    for (auto const& name: profiles) {
        m_profiles->addItem(&newItem(name));
    }
}

void ProfileDialog::
save() const
{
    QStringList profiles;

    for (auto i = 0, len = m_profiles->count(); i < len; ++i) {
        auto const& item = to_ref(m_profiles->item(i));

        profiles << item.text();
    }

    auto& settings = gApp().settings();
    settings.setProfiles(profiles);
}

void ProfileDialog::
addProfile()
{
    auto& item = newItem("New Profile");

    m_profiles->addItem(&item); // QListWidget takes ownership
    m_profiles->setCurrentItem(&item);
    m_profiles->editItem(&item);

    onEdited();
}

void ProfileDialog::
removeProfile()
{
    auto const row = m_profiles->currentRow();

    auto& item = to_ref(m_profiles->takeItem(row));

    auto& settings = gApp().settings();
    settings.beginProfile(item.text());
    settings.remove("");
    settings.endProfile();

    delete &item;

    onEdited();
}

void ProfileDialog::
moveUp()
{
    auto const row = m_profiles->currentRow();
    assert(row > 0);
    auto& item = to_ref(m_profiles->takeItem(row));

    m_profiles->insertItem(row-1, &item);
    m_profiles->setCurrentItem(&item);
}

void ProfileDialog::
moveDown()
{
    auto const row = m_profiles->currentRow();
    assert(row < m_profiles->count()-1);
    auto& item = to_ref(m_profiles->takeItem(row));

    m_profiles->insertItem(row+1, &item);
    m_profiles->setCurrentItem(&item);
}

void ProfileDialog::
updateWidgets()
{
    auto const& selection = m_profiles->selectedItems();

    m_remove->setEnabled(!selection.isEmpty());

    auto& ok = to_ref(m_buttonBox->button(QDialogButtonBox::Ok));
    ok.setEnabled(m_edited);

    auto const row = m_profiles->currentRow();

    auto up = true, down = true;

    if (row < 0) {
        up = down = false;
    }
    else {
        if (row == 0) {
            up = false;
        }

        if (row == m_profiles->count()-1) {
            down = false;
        }
    }

    m_up->setEnabled(up);
    m_down->setEnabled(down);
}

void ProfileDialog::
onAccepted()
{
    save();
    this->accept();
}

void ProfileDialog::
onEdited()
{
    m_edited = true;
    updateWidgets();
}

} // namespace aria2_remote
