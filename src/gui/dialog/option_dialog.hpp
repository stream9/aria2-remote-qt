#ifndef ARIA2_REMOTE_OPTION_DIALOG_HPP
#define ARIA2_REMOTE_OPTION_DIALOG_HPP

#include "option_dialog/option_dialog_base.hpp"

#include "utility/pointer.hpp"

#include <memory>

class QWidget;

namespace aria2_remote {

class Aria2Remote;
class Options;
class ProfileComboBox;

namespace option_dialog {

class Items;

} // namespace option_dialog

class OptionDialog : public OptionDialogBase
{
    Q_OBJECT
public:
    OptionDialog(Aria2Remote&, QWidget& parent);
    ~OptionDialog();

private:
    Items& items() override { return *m_items; }

    Q_SLOT void updateItems();

private:
    qmanaged_ptr<ProfileComboBox> m_profile; // non-null

    std::unique_ptr<option_dialog::Items> m_items; // non-null
    std::unique_ptr<Options> m_profileOptions; // nullable
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_OPTION_DIALOG_HPP
