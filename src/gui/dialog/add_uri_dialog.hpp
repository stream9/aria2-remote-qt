#ifndef ARIA2_REMOTE_ADD_URI_DIALOG_HPP
#define ARIA2_REMOTE_ADD_URI_DIALOG_HPP

#include "add_dialog.hpp"

#include "utility/pointer.hpp"

#include <vector>

class QLineEdit;
class QString;
class QWidget;

namespace aria2_remote {

class AddUriDialog : public AddDialog
{
    Q_OBJECT
public:
    AddUriDialog(QWidget& parent);

    std::vector<QString> uris() const override;

private:
    Q_SLOT void validateUri(QString const& uri);

private:
    qmanaged_ptr<QLineEdit> m_uri; // non-null
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_ADD_URI_DIALOG_HPP
