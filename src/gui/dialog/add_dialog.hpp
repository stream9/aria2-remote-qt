#ifndef ARIA2_REMOTE_ADD_DIALOG_HPP
#define ARIA2_REMOTE_ADD_DIALOG_HPP

#include "utility/pointer.hpp"

#include <QDialog>

class QBoxLayout;
class QDialogButtonBox;
class QLayout;
class QPushButton;
class QString;
class QWidget;

namespace aria2_remote {

class ProfileComboBox;

class AddDialog : public QDialog
{
    Q_OBJECT
public:
    AddDialog(QWidget& parent);

    virtual std::vector<QString> uris() const;
    QString profile() const;

protected:
    QPushButton& additionalUriButton();
    QDialogButtonBox& buttonBox();

private:
    void createProfileWidget(QBoxLayout&);
    void createButtonBox(QBoxLayout&);

    Q_SLOT void queryAdditionalUris();

private:
    qmanaged_ptr<ProfileComboBox> m_profile;
    qmanaged_ptr<QPushButton> m_additionalUriButton;
    qmanaged_ptr<QDialogButtonBox> m_buttonBox;

    QString m_additionalUris;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_ADD_DIALOG_HPP
