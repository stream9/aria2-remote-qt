#ifndef ARIA2_REMOTE_PROFILE_COMBO_BOX_HPP
#define ARIA2_REMOTE_PROFILE_COMBO_BOX_HPP

#include <QComboBox>

class QString;
class QWidget;

namespace aria2_remote {

class ProfileComboBox : public QComboBox
{
    Q_OBJECT
public:
    ProfileComboBox(QWidget& parent, bool includeGlobal = false);

    bool isGlobal() const;

    Q_SIGNAL void profileChanged(QString const& name);

private:
    void load();
    void reload();

    int lastIndex() const;

    Q_SLOT void onActivated(int const index);

private:
    int m_current = 0;
    bool m_includeGlobal = false;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_PROFILE_COMBO_BOX_HPP
