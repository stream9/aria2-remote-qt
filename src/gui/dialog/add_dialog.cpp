#include "add_dialog.hpp"

#include "profile_combo_box.hpp"

#include <cassert>
#include <vector>

#include <QUrl>
#include <QBoxLayout>
#include <QDialogButtonBox>
#include <QInputDialog>
#include <QLabel>
#include <QPushButton>

#include <QDebug>

namespace aria2_remote {

AddDialog::
AddDialog(QWidget& parent)
    : QDialog { &parent }
    , m_profile { make_qmanaged<ProfileComboBox>(*this) }
    , m_additionalUriButton { make_qmanaged<QPushButton>(this) }
    , m_buttonBox { make_qmanaged<QDialogButtonBox>(this) }
{
    auto layout = make_qmanaged<QVBoxLayout>(this);

    createProfileWidget(*layout);
    createButtonBox(*layout);

    this->setMaximumHeight(this->size().height());
}

std::vector<QString> AddDialog::
uris() const
{
    std::vector<QString> results;

    for (auto const& uri: m_additionalUris.split("\n")) {
        auto const& trimmed = uri.trimmed();
        if (!trimmed.isEmpty()) {
            results.push_back(uri);
        }
    }

    return results;
}

QString AddDialog::
profile() const
{
    return m_profile->currentText();
}

QPushButton& AddDialog::
additionalUriButton()
{
    return *m_additionalUriButton;
}

QDialogButtonBox& AddDialog::
buttonBox()
{
    return *m_buttonBox;
}

void AddDialog::
createProfileWidget(QBoxLayout& parent)
{
    auto layout = make_qmanaged<QHBoxLayout>();

    auto label = make_qmanaged<QLabel>("Profile:", this);
    layout->addWidget(label.get());

    layout->addWidget(m_profile.get(), 1);

    parent.addLayout(layout.get());
}

void AddDialog::
createButtonBox(QBoxLayout& parent)
{
    auto layout = make_qmanaged<QHBoxLayout>();

    m_additionalUriButton->setText("Additional URIs");
    layout->addWidget(m_additionalUriButton.get());

    this->connect(m_additionalUriButton.get(), &QPushButton::clicked,
                  this,                        &AddDialog::queryAdditionalUris);

    m_buttonBox->setStandardButtons(
        QDialogButtonBox::Ok | QDialogButtonBox::Cancel);

    auto& ok = to_ref(m_buttonBox->button(QDialogButtonBox::Ok));
    ok.setEnabled(false);

    layout->addWidget(m_buttonBox.get());

    this->connect(m_buttonBox.get(), &QDialogButtonBox::accepted,
                  this,              &AddDialog::accept);
    this->connect(m_buttonBox.get(), &QDialogButtonBox::rejected,
                  this,              &AddDialog::reject);

    parent.addLayout(layout.get());
}

void AddDialog::
queryAdditionalUris()
{
    QString const title { "Additional URIs" };
    QString const label { "Input one URI per line" };
    auto ok = false;

    auto const& lines = QInputDialog::getMultiLineText(
                                this, title, label, m_additionalUris, &ok);
    if (ok) {
        m_additionalUris = lines;
    }
}

} // namespace aria2_remote
