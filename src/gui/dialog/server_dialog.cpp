#include "server_dialog.hpp"

#include "core/server.hpp"
#include "gui/icons.hpp"

#include <cassert>

#include <boost/numeric/conversion/cast.hpp>

#include <QIcon>
#include <QHostInfo>
#include <QAction>
#include <QBoxLayout>
#include <QDialogButtonBox>
#include <QFormLayout>
#include <QLineEdit>
#include <QPushButton>

namespace aria2_remote {

class LineEdit : public QLineEdit
{
    Q_OBJECT
public:
    LineEdit(QWidget& parent)
        : QLineEdit { &parent }
        , m_error { make_qmanaged<QAction>(this) }
    {
        m_error->setIcon(icons::error());
        this->addAction(m_error.get(), QLineEdit::TrailingPosition);
        m_error->setVisible(false);

        this->connect(this, &QLineEdit::editingFinished,
                      this, &LineEdit::validate);
    }

    bool isValid() const { return m_isValid; }

    void setError(QString const& text)
    {
        m_error->setVisible(true);
        m_error->setToolTip(text);
        m_isValid = false;
    }

    void clearError()
    {
        m_error->setVisible(false);
        m_error->setToolTip("");
        m_isValid = true;
    }

    Q_SIGNAL void validated() const;

private:
    virtual Q_SLOT void validate() = 0;

private:
    qmanaged_ptr<QAction> m_error;
    bool m_isValid = false;
};

class NameEdit : public LineEdit
{
    Q_OBJECT
public:
    NameEdit(Servers& servers, QWidget& parent)
        : LineEdit { parent }
        ,m_servers { servers } {}

private:
    void validate() override
    {
        if (this->text().isEmpty()) {
            this->setError("Name must not be empty.");
        }
        else if (m_servers.find(this->text()) != nullptr) {
            this->setError("This name is already taken.");
        }
        else {
            this->clearError();
        }

        Q_EMIT this->validated();
    }

private:
    Servers& m_servers;
};

class HostEdit : public LineEdit
{
    Q_OBJECT
private:
    using LineEdit::LineEdit;

    void validate() override
    {
        QHostInfo::lookupHost(
                this->text(), this, SLOT(onHostLookuped(QHostInfo)));
    }

    Q_SLOT void onHostLookuped(QHostInfo const host)
    {
        if (host.error() != QHostInfo::NoError) {
            this->setError("Can't lookup host");
        }
        else {
            this->clearError();
        }

        Q_EMIT this->validated();
    }
};

class PortEdit : public LineEdit
{
public:
    PortEdit(QWidget& parent)
        : LineEdit { parent }
    {
        this->setText("6800");
        validate();
    }

private:
    void validate() override
    {
        auto ok = false;
        auto const port = this->text().toInt(&ok, 10);
        if (!ok) {
            this->setError("Port must be a number");
        }
        else if (port < 0 || port > 65535) {
            this->setError("Port must be between 0 to 65535");
        }
        else {
            this->clearError();
        }

        Q_EMIT this->validated();
    }
};

ServerDialog::
ServerDialog(QWidget& parent, Servers& servers)
    : QDialog { &parent }
    , m_servers { servers }
    , m_name { make_qmanaged<NameEdit>(m_servers, *this) }
    , m_host { make_qmanaged<HostEdit>(*this) }
    , m_port { make_qmanaged<PortEdit>(*this) }
    , m_token { make_qmanaged<QLineEdit>(this) }
    , m_buttons { make_qmanaged<QDialogButtonBox>(this) }
{
    auto layout = make_qmanaged<QVBoxLayout>(this);

    createForm(*layout);
    createButtonBox(*layout);

    updateWidgets();
}

QString ServerDialog::
name() const
{
    return m_name->text();
}

QString ServerDialog::
host() const
{
    return m_host->text();
}

uint16_t ServerDialog::
port() const
{
    return boost::numeric_cast<uint16_t>(m_port->text().toInt());
}

QString ServerDialog::
token() const
{
    return m_token->text();
}

void ServerDialog::
setServer(Server const& server)
{
    m_name->setText(server.name());
    m_host->setText(server.host());
    m_port->setText(QString::number(server.port()));
    m_token->setText(server.secret());
}

void ServerDialog::
createForm(QBoxLayout& mainLayout)
{
    auto layout = make_qmanaged<QFormLayout>();

    layout->addRow("Name:", m_name.get());
    this->connect(m_name.get(), &LineEdit::validated,
                  this,         &ServerDialog::updateWidgets);

    layout->addRow("Host:", m_host.get());
    this->connect(m_host.get(), &LineEdit::validated,
                  this,         &ServerDialog::updateWidgets);

    layout->addRow("Port:", m_port.get());
    this->connect(m_port.get(), &LineEdit::validated,
                  this,         &ServerDialog::updateWidgets);


    layout->addRow("Token:", m_token.get());

    mainLayout.addLayout(layout.get());
}

void ServerDialog::
createButtonBox(QBoxLayout& mainLayout)
{
    m_buttons->setStandardButtons(
        QDialogButtonBox::Ok | QDialogButtonBox::Cancel);

    this->connect(m_buttons.get(), &QDialogButtonBox::accepted,
                  this,            &ServerDialog::accept);
    this->connect(m_buttons.get(), &QDialogButtonBox::rejected,
                  this,            &ServerDialog::reject);

    mainLayout.addWidget(m_buttons.get());
}

void ServerDialog::
updateWidgets()
{
    auto& ok = to_ref(m_buttons->button(QDialogButtonBox::Ok));

    ok.setEnabled(m_name->isValid() && m_host->isValid() && m_port->isValid());
}

#include "server_dialog.moc"

} // namespace aria2_remote
