#include "handle_option_dialog.hpp"

#include "option_dialog/item.hpp"

#include "core/download_handle.hpp"
#include "core/handle_options.hpp"

#include <QDebug>

namespace aria2_remote {

HandleOptionDialog::
HandleOptionDialog(DownloadHandle& handle, QWidget& parent)
    : OptionDialogBase { handle.aria2(), parent }
    , m_options { std::make_unique<HandleOptions>(handle) }
{
    assert(m_options);

    this->bottomPanel().insertStretch(0, 1);

    this->connect(m_options.get(), &Options::updated,
                  this,            &HandleOptionDialog::onOptionUpdated);

    onOptionUpdated();

    assert(m_items);
}

HandleOptionDialog::~HandleOptionDialog() = default;

void HandleOptionDialog::
onOptionUpdated()
{
    m_items = std::make_unique<Items>(*m_options, this->database(), false);
    assert(m_items);

    this->connect(m_items.get(), &Items::edited,
                  this,          &HandleOptionDialog::onItemEdited);

    this->updateModel();
}

} // namespace aria2_remote
