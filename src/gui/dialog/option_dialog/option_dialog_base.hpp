#ifndef ARIA2_REMOTE_OPTION_DIALOG_BASE_HPP
#define ARIA2_REMOTE_OPTION_DIALOG_BASE_HPP

#include "utility/pointer.hpp"

#include <memory>

#include <QSqlDatabase>
#include <QBoxLayout>
#include <QDialog>

class QCheckBox;
class QComboBox;
class QDialogButtonBox;
class QLineEdit;
class QWidget;

namespace aria2_remote {

class Aria2Remote;
class OptionsFilterModel;
class OptionsModel;
class TreeView;

namespace option_dialog {

class Items;

} // namespace option_dialog

class OptionDialogBase : public QDialog
{
    Q_OBJECT

protected:
    using Items = option_dialog::Items;

public:
    OptionDialogBase(Aria2Remote&, QWidget& parent);
    ~OptionDialogBase();

protected:
    Aria2Remote& aria2() { return m_aria2; }
    QSqlDatabase& database() { return m_database; }
    QHBoxLayout& bottomPanel() { return *m_bottomPanel; }

    void updateModel();

    Q_SLOT void onItemEdited();

private:
    void readSettings();
    void writeSettings() const;

    void createTopPanel(QBoxLayout&);
    void createOptionsView(QBoxLayout&);
    void createBottomPanel(QBoxLayout&);

    void createCategoryCombo();
    QDialogButtonBox& createButtonBox();

    virtual Items& items() = 0;

    Q_SLOT void onCategoryChanged(QString const&);
    Q_SLOT void onFilterEdited(QString const&);
    Q_SLOT void onEditableOnlyChanged(int state);
    Q_SLOT void onModifiedOnlyChanged(int state);
    Q_SLOT void onAccepted();
    Q_SLOT void onFilterShortcut();

private:
    qmanaged_ptr<QComboBox> m_category;
    qmanaged_ptr<QLineEdit> m_filter;
    qmanaged_ptr<TreeView> m_options;
    qmanaged_ptr<QCheckBox> m_editableOnly;
    qmanaged_ptr<QCheckBox> m_modifiedOnly;
    qmanaged_ptr<QDialogButtonBox> m_buttons;
    qmanaged_ptr<QHBoxLayout> m_bottomPanel;

    Aria2Remote& m_aria2;
    QSqlDatabase m_database;
    std::unique_ptr<OptionsModel> m_model; // nullable
    std::unique_ptr<OptionsFilterModel> m_filterModel; // non-null
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_OPTION_DIALOG_BASE_HPP
