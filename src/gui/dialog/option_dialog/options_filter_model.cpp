#include "options_filter_model.hpp"

#include "item.hpp"
#include "options_model.hpp"

#include "utility/pointer.hpp"

#include <cassert>

namespace aria2_remote {

void OptionsFilterModel::
setCategory(QString const& category)
{
    m_category = category;

    this->invalidate();
}

void OptionsFilterModel::
setFilter(QString const& category)
{
    m_filter = category;

    this->invalidate();
}

void OptionsFilterModel::
setEditableOnly(bool const flag)
{
    m_editableOnly = flag;

    this->invalidate();
}

void OptionsFilterModel::
setModifiedOnly(bool const flag)
{
    m_modifiedOnly = flag;

    this->invalidate();
}

bool OptionsFilterModel::
filterAcceptsRow(int const srcRow, QModelIndex const& srcParent) const
{
    assert(!srcParent.isValid()); (void)srcParent;

    auto& srcModel = to_ref(this->sourceModel());
    auto const& srcIndex = srcModel.index(srcRow, 0);

    auto& item = to_ref(
        static_cast<option_dialog::Item*>(srcIndex.internalPointer()));

    if (!m_category.isEmpty() && item.category() != m_category) {
        return false;
    }

    if (!m_filter.isEmpty() && !item.name().contains(m_filter)) {
        return false;
    }

    if (m_editableOnly && !item.isEditable()) {
        return false;
    }

    if (m_modifiedOnly && !item.isModified()) {
        return false;
    }

    return true;
}

} // namespace aria2_remote
