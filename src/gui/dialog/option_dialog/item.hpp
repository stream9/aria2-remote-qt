#ifndef ARIA2_REMOTE_OPTION_DIALOG_ITEMS_HPP
#define ARIA2_REMOTE_OPTION_DIALOG_ITEMS_HPP

#include <QObject>
#include <QString>

#include <vector>

class QSqlDatabase;
class QVariant;

namespace aria2_remote {

class Options;

namespace option_dialog {

class Item
{
public:
    Item(QString const& name
       , QString const& category
       , QString const& value
       , QString const& default_
       , bool editable
    );

    QString const& name() const { return m_name; }
    QString const& category() const { return m_category; }
    QString const& value() const { return m_after; }
    QString const& defaultValue() const { return m_default; }

    bool isModified() const { return value() != m_default; }
    bool isEditable() const { return m_editable; }
    bool isEdited() const { return value() != m_before; }

    void setValue(QString const&);

private:
    QString m_name;
    QString m_category;
    QString m_before;
    QString m_after;
    QString m_default;
    bool m_editable;
};

class Items : public QObject
{
    Q_OBJECT

    using Container = std::vector<Item>;
public:
    Items(Options&, QSqlDatabase&, bool global);

    size_t size() const { return m_items.size(); }

    Item& at(size_t const index) { return m_items.at(index); }
    Item const& at(size_t const index) const { return m_items.at(index); }

    bool isEdited() const;

    void setValue(size_t row, QString const&);

    void commitChange();

    Q_SIGNAL void edited() const;

private:
    Options& m_options;
    Container m_items;
};

}} // namespace aria2_remote::option_dialog

#endif // ARIA2_REMOTE_OPTION_DIALOG_ITEMS_HPP
