#include "options_model.hpp"

#include "item.hpp"

#include <cassert>
#include <optional>

#include <boost/numeric/conversion/cast.hpp>

#include <QModelIndex>
#include <QVariant>
#include <QFont>
#include <QPalette>
#include <QApplication>

#include <QDebug>

namespace aria2_remote {

template<typename T>
T&
validate(T* const ptr)
{
    assert(ptr);
    return *ptr;
}

// OptionsModel

OptionsModel::
OptionsModel(option_dialog::Items& items)
    : m_items { items }
{}

QModelIndex OptionsModel::
index(int const row, int const column,
                     QModelIndex const& parent/*= {}*/) const
{
    assert(row >= 0);
    assert(column >= 0);
    assert(column < Column::Count);
    assert(!parent.isValid()); (void)parent;
    assert(row < rowCount());

    auto& item = m_items.at(static_cast<size_t>(row));

    return this->createIndex(row, column, &item);
}

QModelIndex OptionsModel::
parent(QModelIndex const&) const
{
    return {};
}

int OptionsModel::
rowCount(QModelIndex const& parent/*= {}*/) const
{
    if (parent.isValid()) return 0;

    return boost::numeric_cast<int>(m_items.size());
}

int OptionsModel::
columnCount(QModelIndex const& parent/*= {}*/) const
{
    assert(!parent.isValid()); (void)parent;

    return Column::Count;
}

static option_dialog::Item&
getItem(QModelIndex const& index)
{
    return validate(
            static_cast<option_dialog::Item*>(index.internalPointer()));
}

static QVariant
toVariant(std::optional<QString> const& value)
{
    if (value) {
        return *value;
    }
    else {
        return {};
    }
}

QVariant OptionsModel::
getDisplayData(QModelIndex const& index) const
{
    auto const& item = getItem(index);

    switch (index.column()) {
    case Name:
        return item.name();
    case Category:
        return item.category();
    case Default:
        return toVariant(item.defaultValue());
    case Value:
        return toVariant(item.value());
    default:
        throw std::logic_error("OptionsModel::getDisplayData");
    }
}

QVariant OptionsModel::
getEditData(QModelIndex const& index) const
{
    assert(index.column() == Value);

    auto const& result = getDisplayData(index);

    if (result.canConvert<QString>()) {
        auto const& str = result.toString();

        if (str == "true") {
            return true;
        }
        else if (str == "false") {
            return false;
        }
    }

    return result;
}

QVariant OptionsModel::
getForegroundData(QModelIndex const& index) const
{
    auto const& item = getItem(index);

    if (!item.isEditable()) {
        auto& app = validate(qApp);
        return app.palette().brush(QPalette::Disabled, QPalette::WindowText);
    }

    return {};
}

QVariant OptionsModel::
getFontData(QModelIndex const& index) const
{
    auto const& item = getItem(index);

    if (item.isEdited()) {
        QFont font;
        font.setBold(true);
        return font;
    }

    return {};
}

QVariant OptionsModel::
data(QModelIndex const& index, int const role/*= Qt::DisplayRole*/) const
{
    assert(index.isValid());
    assert(index.row() < rowCount());

    switch (role) {
    case Qt::DisplayRole:
        return getDisplayData(index);
    case Qt::EditRole:
        return getEditData(index);
    case Qt::ForegroundRole:
        return getForegroundData(index);
    case Qt::FontRole:
        return getFontData(index);
    default:
        return {};
    }
}

QVariant OptionsModel::
headerData(int const section, Qt::Orientation const orientation,
                              int const role/*= Qt::DisplayRole*/) const
{
    if (role != Qt::DisplayRole) return {};

    assert(orientation == Qt::Horizontal); (void)orientation;

    switch (section) {
    case Name:
        return "Name";
    case Category:
        return "Category";
    case Default:
        return "Default";
    case Value:
        return "Value";
    default:
        throw std::logic_error("OptionsModel::headerData");
    }
}

Qt::ItemFlags OptionsModel::
flags(QModelIndex const& index) const
{
    assert(index.isValid());

    auto flag = Qt::ItemIsEnabled | Qt::ItemIsSelectable;
    auto const& item = getItem(index);

    if (index.column() == Value && item.isEditable()) {
        flag |= Qt::ItemIsEditable;
    }

    return flag;
}

bool OptionsModel::
setData(QModelIndex const& index,
        QVariant const& value, int const role/* = Qt::EditRole*/)
{
    if (role != Qt::EditRole) return false;
    assert(index.isValid());

    m_items.setValue(boost::numeric_cast<size_t>(index.row()), value.toString());

    QVector<int> roles;
    roles << role;
    Q_EMIT this->dataChanged(index, index, roles);

    return true;
}

} // namespace aria2_remote
