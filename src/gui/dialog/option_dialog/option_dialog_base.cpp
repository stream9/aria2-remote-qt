#include "option_dialog_base.hpp"

#include "item.hpp"
#include "options_filter_model.hpp"
#include "options_model.hpp"

#include "core/application.hpp"
#include "core/aria2_remote.hpp"
#include "core/settings.hpp"
#include "gui/tree_view.hpp"

#include <cassert>

#include <QAction>
#include <QBoxLayout>
#include <QCheckBox>
#include <QComboBox>
#include <QDialogButtonBox>
#include <QDir>
#include <QFileInfo>
#include <QLabel>
#include <QLineEdit>
#include <QPushButton>
#include <QSqlQuery>
#include <QTreeView>
#include <QWidget>

#include <QDebug>

namespace aria2_remote {

static QString
prepareDatabaseFile()
{
    auto const& configDir = Application::configDir();
    auto const& filePath = configDir.filePath("option.db");

    QFileInfo info { filePath };
    if (!info.exists()) {
        if (!QFile::copy(":/option.db", filePath)) {
            qCritical() << "Fail to copy option database";
            assert(false);
        }
    }

    return filePath;
}

static QSqlDatabase
createDatabase()
{
    auto db = QSqlDatabase::database("option");
    if (!db.isValid()) {
        db = QSqlDatabase::addDatabase("QSQLITE", "option");

        auto const& filePath = prepareDatabaseFile();
        db.setDatabaseName(filePath);
        if (!db.open()) {
            qCritical() << "Can't open option database.";
            assert(false);
        }
    }

    return db;
}

OptionDialogBase::
OptionDialogBase(Aria2Remote& aria2, QWidget& parent)
    : QDialog { &parent }
    , m_category { make_qmanaged<QComboBox>(this) }
    , m_filter { make_qmanaged<QLineEdit>(this) }
    , m_options { make_qmanaged<TreeView>() }
    , m_editableOnly { make_qmanaged<QCheckBox>(this) }
    , m_modifiedOnly { make_qmanaged<QCheckBox>(this) }
    , m_buttons { make_qmanaged<QDialogButtonBox>(this) }
    , m_bottomPanel { make_qmanaged<QHBoxLayout>() }
    , m_aria2 { aria2 }
    , m_database { createDatabase() }
    , m_filterModel { std::make_unique<OptionsFilterModel>() }
{
    auto layout = make_qmanaged<QVBoxLayout>(this);

    createTopPanel(*layout);

    createOptionsView(*layout);

    createBottomPanel(*layout);

    readSettings();
}

OptionDialogBase::
~OptionDialogBase()
{
    writeSettings();
}

void OptionDialogBase::
readSettings()
{
    auto& settings = gApp().settings();

    settings.beginGroup("OptionDialog");

    if (auto value = settings.value("width"); !value.isNull()) {
        this->resize(value.toInt(), this->height());
    }

    if (auto value = settings.value("height"); !value.isNull()) {
        this->resize(this->width(), value.toInt());
    }

    m_category->setCurrentText(settings.value("category").toString());
    m_editableOnly->setCheckState(
        static_cast<Qt::CheckState>(settings.value("editableOnly").toInt()));
    m_modifiedOnly->setCheckState(
        static_cast<Qt::CheckState>(settings.value("modifiedOnly").toInt()));

    settings.endGroup();

    m_options->readSettings();
}

void OptionDialogBase::
writeSettings() const
{
    auto& settings = gApp().settings();

    settings.beginGroup("OptionDialog");

    settings.setValue("category", m_category->currentText());
    settings.setValue("editableOnly", m_editableOnly->checkState());
    settings.setValue("modifiedOnly", m_modifiedOnly->checkState());
    settings.setValue("width", this->width());
    settings.setValue("height", this->height());

    settings.endGroup();

    m_options->writeSettings();
}

void OptionDialogBase::
createTopPanel(QBoxLayout& mainLayout)
{
    auto layout = make_qmanaged<QHBoxLayout>();

    auto label1 = make_qmanaged<QLabel>("Category:", this);
    layout->addWidget(label1.get());

    createCategoryCombo();
    layout->addWidget(m_category.get());

    this->connect(m_category.get(), &QComboBox::currentTextChanged,
                  this,             &OptionDialogBase::onCategoryChanged);

    m_filter->setClearButtonEnabled(true);
    m_filter->setPlaceholderText("Filter");

    auto shortcut = make_qmanaged<QAction>(m_filter);
    shortcut->setShortcut(Qt::CTRL | Qt::Key_F);

    this->connect(shortcut.get(), &QAction::triggered,
                  this,           &OptionDialogBase::onFilterShortcut);

    m_filter->addAction(shortcut.get());

    layout->addWidget(m_filter.get(), 1);

    this->connect(m_filter.get(), &QLineEdit::textEdited,
                  this,           &OptionDialogBase::onFilterEdited);

    mainLayout.addLayout(layout.get());
}

void OptionDialogBase::
onFilterShortcut()
{
    m_filter->setFocus(Qt::ShortcutFocusReason);
}

void OptionDialogBase::
createOptionsView(QBoxLayout& mainLayout)
{
    m_options->setObjectName("OptionsView");
    m_options->setModel(m_filterModel.get());
    m_options->setRootIsDecorated(false);
    m_options->setAlternatingRowColors(true);
    m_options->setSortingEnabled(true);

    mainLayout.addWidget(m_options.get());
}

void OptionDialogBase::
createBottomPanel(QBoxLayout& mainLayout)
{
    m_editableOnly->setText("Editable");
    m_bottomPanel->addWidget(m_editableOnly.get());

    this->connect(m_editableOnly.get(), &QCheckBox::stateChanged,
                  this,                 &OptionDialogBase::onEditableOnlyChanged);

    m_modifiedOnly->setText("Modified");
    m_bottomPanel->addWidget(m_modifiedOnly.get());

    this->connect(m_modifiedOnly.get(), &QCheckBox::stateChanged,
                  this,                 &OptionDialogBase::onModifiedOnlyChanged);

    m_bottomPanel->addWidget(m_buttons.get());

    mainLayout.addLayout(m_bottomPanel.get());
}

void OptionDialogBase::
createCategoryCombo()
{
    m_category->addItem("All");
    m_filterModel->setCategory("");

    QSqlQuery query { "SELECT distinct category FROM option", m_database };
    query.exec();
    while (query.next()) {
        m_category->addItem(query.value("category").toString());
    }
}

QDialogButtonBox& OptionDialogBase::
createButtonBox()
{
    auto buttons = make_qmanaged<QDialogButtonBox>(
        QDialogButtonBox::Ok | QDialogButtonBox::Cancel,
        this
    );

    auto& ok = to_ref(buttons->button(QDialogButtonBox::Ok));
    ok.setEnabled(false);

    this->connect(buttons.get(), &QDialogButtonBox::accepted,
                  this,          &OptionDialogBase::onAccepted);

    this->connect(buttons.get(), &QDialogButtonBox::rejected,
                  this,          &OptionDialogBase::reject);

    return *buttons;
}

void OptionDialogBase::
updateModel()
{
    auto& items = this->items();

    m_model = std::make_unique<OptionsModel>(items);
    assert(m_model);

    m_filterModel->setSourceModel(m_model.get());
}

void OptionDialogBase::
onCategoryChanged(QString const& category)
{
    if (category == "All") {
        m_filterModel->setCategory("");
    }
    else {
        m_filterModel->setCategory(category);
    }
}

void OptionDialogBase::
onFilterEdited(QString const& filter)
{
    m_filterModel->setFilter(filter);
}

void OptionDialogBase::
onEditableOnlyChanged(int const state)
{
    m_filterModel->setEditableOnly(state != Qt::Unchecked);
}

void OptionDialogBase::
onModifiedOnlyChanged(int const state)
{
    m_filterModel->setModifiedOnly(state != Qt::Unchecked);
}

void OptionDialogBase::
onAccepted()
{
    items().commitChange();
    this->accept();
}

void OptionDialogBase::
onItemEdited()
{
    auto& ok = to_ref(m_buttons->button(QDialogButtonBox::Ok));
    ok.setEnabled(items().isEdited());
}

} // namespace aria2_remote
