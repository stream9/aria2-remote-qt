#include "property_dialog.hpp"

#include "core/application.hpp"
#include "core/download_handle.hpp"

#include "gui/main_window.hpp"
#include "gui/panel/files_panel.hpp"
#include "gui/panel/general_panel.hpp"
#include "gui/panel/peers_panel.hpp"
#include "gui/panel/uris_panel.hpp"

#include <QBoxLayout>
#include <QDialogButtonBox>
#include <QTabWidget>

namespace aria2_remote {

PropertyDialog::
PropertyDialog()
    : QDialog { &gApp().mainWindow() }
    , m_tab { make_qmanaged<QTabWidget>(this) }
    , m_buttonBox { make_qmanaged<QDialogButtonBox>(this) }
{
    auto layout = make_qmanaged<QVBoxLayout>(this);

    auto general = make_qmanaged<GeneralPanel>();
    m_tab->addTab(general.get(), "General");

    auto files = make_qmanaged<FilesPanel>();
    m_tab->addTab(files.get(), "Files");

    if (gApp().currentHandle() && gApp().currentHandle()->isTorrent()) {
        auto peers = make_qmanaged<PeersPanel>();

        m_tab->addTab(peers.get(), "Peers");
    }

    auto uris = make_qmanaged<UrisPanel>();
    m_tab->addTab(uris.get(), "URIs");

    layout->addWidget(m_tab.get());

    m_buttonBox->addButton(QDialogButtonBox::Ok);
    layout->addWidget(m_buttonBox.get());

    this->connect(m_buttonBox.get(), &QDialogButtonBox::accepted,
                  this,              &QDialog::accept);
    this->connect(m_buttonBox.get(), &QDialogButtonBox::rejected,
                  this,              &QDialog::reject);
}

} // namespace aria2_remote
