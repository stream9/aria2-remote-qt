#ifndef ARIA2_REMOTE_JSON_RPC_RESPONSE_HPP
#define ARIA2_REMOTE_JSON_RPC_RESPONSE_HPP

#include <QJsonObject>

class QJsonValue;

namespace json_rpc {

class Response
{
public:
    Response() = delete;

    bool isError() const;
    operator bool() const { return !isError(); }

    QJsonValue value() const;
    QJsonValue error() const;

    static Response fromValue(QJsonValue const&);
    static Response fromError(QJsonValue const&);

private:
    Response(QJsonObject const&);

private:
    QJsonObject m_obj;

    friend class WebSocket;
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_JSON_RPC_RESPONSE_HPP
