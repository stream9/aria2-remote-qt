#ifndef ARIA2_REMOTE_JSON_RPC_TYPES_HPP
#define ARIA2_REMOTE_JSON_RPC_TYPES_HPP

#include <cstdint>
#include <functional>

namespace json_rpc {

class WebSocket;
class Response;

using Id = int64_t;
using Callback = std::function<void(Response const&)>;

} // namespace json_rpc

#endif // ARIA2_REMOTE_JSON_RPC_TYPES_HPP
