#include "response.hpp"

#include <cassert>

#include <QJsonValue>

namespace json_rpc {

Response::
Response(QJsonObject const& obj)
    : m_obj { obj }
{}

bool Response::
isError() const { return m_obj.contains("error"); }

QJsonValue Response::
value() const
{
    auto const result = m_obj.value("result");
    assert(!result.isUndefined());

    return result;
}

QJsonValue Response::
error() const
{
    auto const result = m_obj.value("error");
    assert(!result.isUndefined());

    return result;
}

Response Response::
fromValue(QJsonValue const& value)
{
    QJsonObject obj;
    obj["result"] = value;

    return obj;
}

Response Response::
fromError(QJsonValue const& error)
{
    QJsonObject obj;
    obj["error"] = error;

    return obj;
}

} // namespace json_rpc
