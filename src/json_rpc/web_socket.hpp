#ifndef ARIA2_REMOTE_JSON_RPC_HPP
#define ARIA2_REMOTE_JSON_RPC_HPP

#include "types.hpp"

#include <memory>
#include <unordered_map>

#include <QJsonValue>
#include <QObject>

class QJsonArray;
class QJsonObject;
class QString;
class QWebSocket;

namespace json_rpc {

class WebSocket : public QObject
{
    Q_OBJECT
public:
    WebSocket(QUrl const&);
    ~WebSocket();

    void call(QString const& method, QJsonArray const& params, Callback const&);
    void call(QString const& method, QJsonObject const& params, Callback const&);

    bool isValid() const;

public:
    Q_SIGNAL void connected();
    Q_SIGNAL void disconnected();
    Q_SIGNAL void error(QString const&);
    Q_SIGNAL void notification(QString const& method, QJsonValue const& params);

private:
    Q_SLOT void onError();
    Q_SLOT void onTextMessageReceived(QString const&);

    Id nextId();
    void handleResponse(QJsonObject const&);

private:
    std::unique_ptr<QWebSocket> m_webSocket;
    std::unordered_map<Id, Callback> m_callbacks;
    Id m_serial = 0;
};

} // namespace json_rpc

#endif // ARIA2_REMOTE_JSON_RPC_HPP
