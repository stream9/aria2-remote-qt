#ifndef ARIA2_REMOTE_VALID_MODEL_INDEX_HPP
#define ARIA2_REMOTE_VALID_MODEL_INDEX_HPP

#include <cassert>

#include <QModelIndex>

namespace aria2_remote {

class ConstValidModelIndex
{
public:
    ConstValidModelIndex(QModelIndex const& index) noexcept
        : m_index { &index }
    {
        assert(m_index);
        assert(m_index->isValid()); // imply following 3 assertion
        //assert(m_index->row() >= 0);
        //assert(m_index->column() >= 0);
        //assert(m_index->model());

        auto const& model = *m_index->model();
        assert(m_index->row() < model.rowCount()); (void)model;
        assert(m_index->column() < model.columnCount());
    }

    // accessors
    operator QModelIndex const*() const noexcept { return m_index; }
    QModelIndex const* operator->() const noexcept { return m_index; }
    QModelIndex const& operator*() const noexcept { return *m_index; }

private:
    QModelIndex const* const m_index; // non-null
};

} // namespace aria2_remote

#endif // ARIA2_REMOTE_VALID_MODEL_INDEX_HPP
