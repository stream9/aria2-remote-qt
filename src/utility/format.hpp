#ifndef ARIA2_REMOTE_UTILITY_HPP
#define ARIA2_REMOTE_UTILITY_HPP

#include <cstdint>

class QString;

namespace aria2_remote::util {

QString formatTime(uint64_t const sec);

QString formatBandwidth(uint64_t const bytePerSec);

QString formatSize(uint64_t const byte);

} // namespace aria2_remote::util

#endif // ARIA2_REMOTE_UTILITY_HPP
