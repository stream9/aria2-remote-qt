#include "json.hpp"

#include <cassert>

#include <boost/numeric/conversion/cast.hpp>

#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QString>

#include <QDebug>

namespace aria2_remote::util {

static QString
toString(bool const value)
{
    return value ? "true" : "false";
}

static QString
toString(double const value)
{
    return QString::number(value);
}

bool
toBool(QJsonValue const& value)
{
    if (value.isBool()) {
        return value.toBool();
    }
    else if (value.isString()) {
        auto const& str = value.toString();

        if (str.compare("true", Qt::CaseInsensitive) == 0) {
            return true;
        }
        else if (str.compare("false", Qt::CaseInsensitive) == 0) {
            return false;
        }
    }

    qDebug() << "Can't convert to bool:" << value;
    assert(false);
}

template<typename T>
T toNumber(QJsonValue const& value)
{
    if (value.isDouble()) {
        return boost::numeric_cast<T>(value.toDouble());
    }
    else if (value.isString()){
        auto const& str = value.toString();

        bool ok = false;
        auto const dbl = str.toDouble(&ok);

        if (!ok) {
            qDebug() << __PRETTY_FUNCTION__
                     << " can't convert string to number:" << str;

            throw std::runtime_error { "json::toNumber() conversion error" };
        }

        return boost::numeric_cast<T>(dbl);
    }
    else {
        qDebug() << __PRETTY_FUNCTION__
                 << " can't convert to number:" << value;

        throw std::runtime_error { "json::toNumber() conversion error" };
    }
}

int16_t
toInt16(QJsonValue const& value)
{
    return toNumber<int16_t>(value);
}

uint16_t
toUInt16(QJsonValue const& value)
{
    return toNumber<uint16_t>(value);
}

int32_t
toInt32(QJsonValue const& value)
{
    return toNumber<int32_t>(value);
}

int64_t
toInt64(QJsonValue const& value)
{
    return toNumber<int64_t>(value);
}

QString
toString(QJsonValue const& value)
{
    assert(value.isString());
    return value.toString();
}

QJsonObject
toObject(QJsonValue const& value)
{
    assert(value.isObject());
    return value.toObject();
}

QJsonArray
toArray(QJsonValue const& value)
{
    if (!value.isArray()) {
        qDebug() << value;
        assert(false);
    }
    return value.toArray();
}

template<>
bool
convert(QJsonValue const& value)
{
    return toBool(value);
}

template<>
int32_t
convert(QJsonValue const& value)
{
    return toInt32(value);
}

template<>
int64_t
convert(QJsonValue const& value)
{
    return toInt64(value);
}

template<>
QString
convert(QJsonValue const& value)
{
    return toString(value);
}

template<>
QJsonObject
convert(QJsonValue const& value)
{
    return toObject(value);
}

template<>
QJsonArray
convert(QJsonValue const& value)
{
    return toArray(value);
}

QString
toJson(QJsonValue const& value, QJsonDocument::JsonFormat const format)
{
    using T = QJsonValue::Type;

    switch (value.type()) {
        case T::Null:
            return "null";
        case T::Bool:
            return toString(value.toBool());
        case T::Double:
            return toString(value.toDouble());
        case T::String:
            return value.toString();
        case T::Array:
            return QJsonDocument(value.toArray()).toJson(format);
        case T::Object:
            return QJsonDocument(value.toObject()).toJson(format);
        case T::Undefined:
            return "undefined";
    }

    return "unknown type";
}

} // namespace aria2_remote::util
